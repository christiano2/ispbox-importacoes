import { dbConnect } from "./common";
import { models, setupModels } from "./models/models";
import { mkauth as config } from "../config.json";
import * as DadosClientes from "./dados/Clientes";
import * as DadosPlanos from "./dados/Planos";
import * as DadosServicosInternet from "./dados/ServicosInternet";
import * as DadosCobrancas from "./dados/Cobrancas";
import { Model } from "../lib/model";

// primeiro iniciamos a conexão
export const dbConnection = dbConnect(config.database);

// setupModels();
Model.initModels();

export { models };
 
export function run() {

    // DadosClientes.clear();
    // DadosPlanos.clear();
    // DadosServicosInternet.clear();
    //
    //
    // DadosPlanos.migra();
    // DadosClientes.migra();
    // // migra os adicionais pois os serviços são migrados junto com os dados dos clientes
    // // assim evitamos selecionar 2x os registros dos clientes
    // DadosServicosInternet.migra();

    DadosCobrancas.clear();
    DadosCobrancas.migra();

}

