import { base as ModelBase, MunicipiosIbge, Clientes, ClientesContatos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import SisCliente from "../models/SisCliente";
import { numerico, data } from "powerjs/support/helper";
import * as Tools from "../../lib/tools";
import console_tools from "powerjs/support/console_tools";
import * as DadosServicosInternet from "./ServicosInternet";

export function clear() {
    console.log('Limpando dados de clientes...');
    // setamos o conn para usar a conexão padrao do mvc
    conn.setDb(ModelBase.getConnection('duobox'));

    conn.beginTransaction();
    try {
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes');
        conn.exec('TRUNCATE clientes_contatos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "CLIENTE"');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
        console.log('Tabela de clientes truncada');
    } catch(err) {
        conn.rollback();
        console.log(`Erro limpado clientes: ${err.getMessage()}`);
        console.log(err);
        exit();
    }
}

export function migra() {
    // let subs: SisCliente[] = SisCliente.all('', false, { 'limit': 10, 'offset': 0 });
    console.log('Buscando cadastros de clientes');
    let subs: SisCliente[] = SisCliente.all();
    let totalSubs = subs.length;
    let actualSub = 0;

    let ddd = '87';

    let estadoCivil = {
        'S': 1,
        'C': 2,
        'D': 3,
        'V': 4,
        'default': 0
    }

    for (let subIndex in subs) {
        let sub = subs[subIndex];
        console_tools.mostraProgresso(++actualSub, totalSubs, `Migrando cliente ${sub.id} - ${sub.nome}`);
        let cliente = new Clientes();
        let contato = null;
        let celular3 = '';
        sub.celular = numerico.apenasNumero(sub.celular ?? '');
        sub.fone = numerico.apenasNumero(sub.fone ?? '');
        if (sub.celular.length == 16) {
            celular3 = Tools.telefone(sub.celular.substring(8), ddd);
            sub.celular = sub.celular.substring(0,8);
        }
        sub.fone = Tools.telefone(sub.fone, ddd);
        sub.celular = Tools.telefone(sub.celular, ddd);
        cliente.id = sub.id;
        cliente.nome = sub.nome;

        let nascimentoRegex = /\d{2}-\d{2}-\d{4}/;

        if (sub.nascimento.match(nascimentoRegex)) {
            cliente.data_nascimento = data.converToData(sub.nascimento, 'd-m-Y', data.DATA_DB);
        }
        else {
            cliente.data_nascimento = sub.nascimento ? data.convertDataBrToDb(sub.nascimento) : '0000-00-00';
        }
        cliente.sexo = 'NI';

        cliente.estado_civil = estadoCivil[sub.estado_civil] || estadoCivil['default'];
        cliente.nome_pai = '';
        cliente.nome_mae = '';
        cliente.inscricao_municipal = '';

        if (sub.cpf_cnpj.length <= 11) {
            cliente.tipo_pessoa = Clientes.TIPO_PESSOA_FISICA;
            cliente.cpf = numerico.apenasNumero(sub.cpf_cnpj ?? '');
            cliente.rg = sub.rg ? `${sub.rg}${sub.expedicao_rg}` : '';
            cliente.nome_pai = sub.nome_pai ?? '';
            cliente.nome_mae = sub.nome_mae ?? '';
        }
        else {
            cliente.tipo_pessoa = Clientes.TIPO_PESSOA_JURIDICA;
            cliente.cnpj = numerico.apenasNumero(sub.cpf_cnpj ?? '');
            cliente.inscricao_estadual = sub.rg;
        }

        cliente.exterior = 0;
        cliente.documento_exterior = 0;
        cliente.responsavel = '';
        cliente.ruc = '';
        cliente.pais = '';
        cliente.profissao = '';
        cliente.local_trabalho = '';
        cliente.produtor_rural = 0;
        cliente.email = sub.email;
        cliente.email2 = null;
        cliente.telefone = sub.fone;
        cliente.celular = sub.celular;
        if (celular3 != '') {
            if (sub.fone == '') {
                sub.fone = celular3;
            } else if (sub.celular = '') {
                sub.celular = celular3;
            } else {
                contato = new ClientesContatos();
                contato.clientes_id = cliente.id;
                contato.nome = cliente.nome;
                contato.telefone = celular3;
                contato.observacoes = '';
            }
        }

        cliente.cep = Tools.cep(sub.cep_res);
        cliente.endereco = sub.endereco;
        cliente.endereco_numero = sub.numero;
        cliente.endereco_complemento = sub.complemento;
        cliente.endereco_referencia = sub.dot_ref ?? '';
        cliente.endereco_bairro = sub.bairro;
        cliente.cidade = sub.cidade;
        cliente.uf = sub.estado;
        cliente.coordenadas_gps = sub.coordenadas ?? '';
        cliente.cidade_ibge = '';

        let ibge = MunicipiosIbge.first('uf_sigla = ? AND municipio_nome = ?', [
            cliente.uf,
            cliente.cidade
        ]);

        if (ibge) {
            cliente.cidade_ibge = `${ibge.uf_codigo}${ibge.municipio_codigo}`;
        }

        cliente.cobranca_igual = 1;
        cliente.cobranca_endereco_referencia = '';
        cliente.cobranca_cidade_ibge = '';
        cliente.cobranca_pais = '';
        cliente.cobranca_coordenadas_gps = '';

        cliente.cadastro_usuarios_id = 0;
        cliente.alteracao_usuarios_id = 0;

        cliente.documento_bloqueado_observacao = '';

        cliente.data_cadastro = sub.cadastro ? data.convertDataBrToDb(sub.cadastro) : '0000-00-00';
        cliente.data_alteracao = '0000-00-00';

        try {
            cliente.setLogSistema(false);
            cliente.Insert();
            if (contato) {
                contato.Insert();
            }
        } catch (e) {
            var_dump(e.getMessage());
            exit();
        }
        // chamada para inserir o serviço
        DadosServicosInternet.insereServico(sub);
    }
    console.log();
}
