import { base as ModelBase, Internetplanos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import SisPlano from "../models/SisPlano.ts";
import console_tools from "powerjs/support/console_tools";

export function clear()
{
    conn.setDb(ModelBase.getConnection('duobox'));
    console.log('Limpando planos...');
    try {
        conn.beginTransaction();
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE internetplanos');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado planos: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabela de planos truncada');
}

export function migra() {
    let planos: SisPlano[] = SisPlano.all();
    let totalPlans = planos.length;
    let actualPlan = 0;
    for (let planoIdx in planos) {
        let plano = planos[planoIdx];
        console_tools.mostraProgresso(++actualPlan, totalPlans, `Migrando plano ${plano.nome}`);
        let planoNovo = new Internetplanos();
        planoNovo.nome = plano.nome;
        planoNovo.velocidade_down = plano.veldown;
        planoNovo.velocidade_down_garantia = (plano.gardown > 0) ? plano.gardown : plano.veldown;
        planoNovo.velocidade_up = plano.velup;
        planoNovo.velocidade_up_garantia = (plano.garup > 0) ? plano.garup : plano.velup;
        planoNovo.prioridade = plano.prioridade;
        planoNovo.nome_pool = '';
        planoNovo.descricao_nota_fiscal = plano.desc_titulo;
        planoNovo.valor_scm = 0;
        planoNovo.valor_sva = 0;
        planoNovo.valor_servico = 0;
        planoNovo.valor_iva = 0;
        planoNovo.instrucoes_boleto = '';

        planoNovo.ciscoiosxe_nome_pool = '';
        planoNovo.ciscoiosxe_nome_pool_v6 = '';

        planoNovo.horario_ciscoiosxe_nome_pool = '';
        planoNovo.horario_ciscoiosxe_nome_pool_v6 = '';
        planoNovo.franquia_dados_ciscoiosxe_nome_pool = '';
        planoNovo.franquia_dados_ciscoiosxe_nome_pool_v6 = '';
        planoNovo.suspensao_parcial_ciscoiosxe_nome_pool = '';
        planoNovo.suspensao_parcial_ciscoiosxe_nome_pool_v6 = '';

        planoNovo.ciscoios_nome_pool = '';
        planoNovo.horario_ciscoios_nome_pool = '';
        planoNovo.franquia_dados_ciscoios_nome_pool = '';
        planoNovo.suspensao_parcial_ciscoios_nome_pool = '';

        planoNovo.huaweine_nome_pool = '';
        planoNovo.huaweine_nome_pool_v6 = '';
        planoNovo.horario_huaweine_nome_pool = '';
        planoNovo.horario_huaweine_nome_pool_v6 = '';
        planoNovo.franquia_dados_huaweine_nome_pool = '';
        planoNovo.franquia_dados_huaweine_nome_pool_v6 = '';
        planoNovo.suspensao_parcial_huaweine_nome_pool = '';
        planoNovo.suspensao_parcial_huaweine_nome_pool_v6 = '';

        planoNovo.huaweine_dns1 = '';
        planoNovo.huaweine_dns2 = '';

        planoNovo.junipermx_nome_pool = '';
        planoNovo.junipermx_nome_pool_v6 = '';
        planoNovo.horario_junipermx_nome_pool = '';
        planoNovo.horario_junipermx_nome_pool_v6 = '';
        planoNovo.franquia_dados_junipermx_nome_pool = '';
        planoNovo.franquia_dados_junipermx_nome_pool_v6 = '';
        planoNovo.suspensao_parcial_junipermx_nome_pool = '';
        planoNovo.suspensao_parcial_junipermx_nome_pool_v6 = '';

        planoNovo.lista_dinamica = '';

        if (!planoNovo.prioridade)
        {
            planoNovo.prioridade = 8;
        }

        planoNovo.burst_down = 0;
        planoNovo.burst_down_threshold = 0;
        planoNovo.burst_down_time = 0;
        planoNovo.burst_up = 0;
        planoNovo.burst_up_threshold = 0;
        planoNovo.burst_up_time = 0;

        if (plano.burst == 'sim') {
            planoNovo.burst_up = plano.maxup;
            planoNovo.burst_up_threshold = plano.desaup;
            planoNovo.burst_up_time = plano.tempoup;
            planoNovo.burst_down = plano.maxdown;
            planoNovo.burst_down_threshold = plano.desadown;
            planoNovo.burst_down_time = plano.tempodown;
        }

        planoNovo.suspensao_parcial_velocidade_down = 1;
        planoNovo.suspensao_parcial_velocidade_down_garantia = 1;
        planoNovo.suspensao_parcial_velocidade_up = 1;
        planoNovo.suspensao_parcial_velocidade_up_garantia = 1;
        planoNovo.suspensao_parcial_prioridade = 8;
        planoNovo.suspensao_parcial_nome_pool = '';
        planoNovo.suspensao_parcial_lista_dinamica = '';
        planoNovo.suspensao_parcial_burst_down = 0;
        planoNovo.suspensao_parcial_burst_down_threshold = 0;
        planoNovo.suspensao_parcial_burst_down_time = 0;
        planoNovo.suspensao_parcial_burst_up = 0;
        planoNovo.suspensao_parcial_burst_up_threshold = 0;
        planoNovo.suspensao_parcial_burst_up_time = 0;
//            if (plano.late_payment_reduce) {
//                planoNovo.suspensao_parcial_velocidade_down = plano.late_payment_speed_down;
//                planoNovo.suspensao_parcial_velocidade_down_garantia = plano.late_payment_speed_down;
//                planoNovo.suspensao_parcial_velocidade_up = plano.late_payment_speed_up;
//                planoNovo.suspensao_parcial_velocidade_up_garantia = plano.late_payment_speed_up;
//                planoNovo.suspensao_parcial_prioridade = 8;
//            }

        planoNovo.horario_velocidade_down = 1;
        planoNovo.horario_velocidade_down_garantia = 1;
        planoNovo.horario_velocidade_up = 1;
        planoNovo.horario_velocidade_up_garantia = 1;
        planoNovo.horario_prioridade = 8;
        planoNovo.horario_nome_pool = '';
        planoNovo.horario_lista_dinamica = '';
        planoNovo.horario_burst_down = 0;
        planoNovo.horario_burst_down_threshold = 0;
        planoNovo.horario_burst_down_time = 0;
        planoNovo.horario_burst_up = 0;
        planoNovo.horario_burst_up_threshold = 0;
        planoNovo.horario_burst_up_time = 0;

        planoNovo.franquia_dados_ativada = 0;
        planoNovo.franquia_dados = 1024;
        planoNovo.franquia_dados_velocidade_down = 1;
        planoNovo.franquia_dados_velocidade_down_garantia = 1;
        planoNovo.franquia_dados_velocidade_up = 1;
        planoNovo.franquia_dados_velocidade_up_garantia = 1;
        planoNovo.franquia_dados_prioridade = 8;
        planoNovo.franquia_dados_nome_pool = '';
        planoNovo.franquia_dados_lista_dinamica = '';
        planoNovo.franquia_dados_burst_down = 0;
        planoNovo.franquia_dados_burst_down_threshold = 0;
        planoNovo.franquia_dados_burst_down_time = 0;
        planoNovo.franquia_dados_burst_up = 0;
        planoNovo.franquia_dados_burst_up_threshold = 0;
        planoNovo.franquia_dados_burst_up_time = 0;

        planoNovo.desconto_antecipacao = plano.valor_desc;
        planoNovo.valor = plano.valor;

        try {
            planoNovo.setLogSistema(false);
            planoNovo.Insert();
        }
        catch (e) {
            var_dump(e.getMessage());
            exit();
        }
    }
    console.log();
}
