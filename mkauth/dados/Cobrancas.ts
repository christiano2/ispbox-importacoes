import { base as ModelBase, Cobrancas, CobrancasBoleto, CobrancasParcelas, Bancos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import SisLanc from "../models/SisLanc";
import { data } from "powerjs/support/helper";
import console_tools from "powerjs/support/console_tools";
import codigobarra from "powerjs/cobrancas/boletos/codigobarra";
import SisCliente from "../models/SisCliente";
import SisBoleto from "../models/SisBoleto";
import ThreadManager from "powerjs/ThreadManager";
import { dbConnect, baseNamespace } from "../common"
import { mkauth as config } from "../../config.json";
import {Model} from "../../lib/model";

let listaBancos: any;

export function clear() {
    conn.setDb(ModelBase.getConnection("duobox"));
    console.log('Limpando cobrancas...');
    try {
        conn.beginTransaction();
        conn.disableFk();
        conn.exec('TRUNCATE cobrancas');
        conn.exec('TRUNCATE cobrancas_boleto');
        conn.exec('TRUNCATE cobrancas_parcelas');
        conn.exec('DELETE FROM contas_movimentacao WHERE documento_tipo = "BOLETO"');
        conn.enableFk();
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado cobrancas: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabelas de cobrancas truncadas');
}


function connectDb()
{
    conn.createNewConnection();
    dbConnect(config.database);
}

function disconnectDb()
{
    let connMkauth = ModelBase.getConnection('duobox');
    let connIspbox = ModelBase.getConnection(baseNamespace);

    connMkauth.Disconnect();
    connIspbox.Disconnect();
}

function migraLote(pagina, linhasPagina, consoleBaseLine) {
    connectDb();
    pagina = parseInt(pagina)-1;
    let limitStart = pagina * linhasPagina;
    let limitEnd = (pagina+1) * linhasPagina;
    let offsetInc = 500;
    let linhaConsole = parseInt(consoleBaseLine) + (pagina * 2) + 1;
    conn.setDb(ModelBase.getConnection(baseNamespace));
    let rowTotal = parseInt(conn.first(`SELECT COUNT(*) AS total FROM (SELECT id FROM sis_lanc LIMIT ${limitStart},${linhasPagina}) AS a`).total);
    let rowCount = 0;

    while (limitStart < limitEnd) {
        // verificamos se estamos no final do lote e ajustamos o offsetInc para não pegar registros da página seguinte
        if ((limitStart+offsetInc) > limitEnd) {
            offsetInc -= limitStart+offsetInc-limitEnd;
        }

        let lancamentos: SisLanc[] = SisLanc.all(`ORDER BY id ASC LIMIT ${limitStart},${offsetInc}`);

        for (let lancamentoIndex in lancamentos) {
            rowCount++;
            let b = lancamentos[lancamentoIndex];

            console_tools.mostraProgresso(rowCount, rowTotal, `Lançamento ID: ${b.id}`, linhaConsole);
            // se não tem assinante não migramos
            if (!b.ClientesHasInternetplanos) {
                continue;
            }
            let cobrancaBoleto = null;
            let cobrancaParcela = null;
            let cobranca = new Cobrancas();
            cobranca.id = b.id;
            cobranca.clientes_id = b.ClientesHasInternetplanos.clientes_id;
            // existem boletos vinculados com contas que não existem mais
            // então se o boleto está vinculado com uma conta inexistente
            // jogamos para a conta 1 do ispbox que é a cobrança em carteira
            cobranca.bancos_id = listaBancos[b.numconta] ? parseInt(b.numconta) + 1 : 1;
            cobranca.origem_tipo = 'INTERNET';
            cobranca.origem_id = b.ClientesHasInternetplanos.id;

            cobranca.valor = b.valor;
            cobranca.valor_pago = (b.status == 'pago') ? b.valorpag : 0;

            cobranca.valor_mensalidade = (b.tipo == 'mensalidade') ? b.valor : 0;

            cobranca.metodo_cobranca = Cobrancas.METODO_BOLETO;

            cobranca.nossonumero = b.nossonum;
            cobranca.sequencial = b.nossonum;
            //
            if (b.linhadig) {
                let parse = codigobarra.parseBancoLinhaDigitavel(b.linhadig);
                cobranca.nossonumero = parse.getNossoNumero() + parse.getDvNossoNumero();
                cobranca.sequencial = parse.getSequencial();
            }

            cobranca.referencia_mensalidade = null;
            cobranca.referencia_mensalidade_inicio = null;
            cobranca.referencia_mensalidade_fim = null;
            cobranca.tipo = 3;
            cobranca.registrado = 1;
            cobranca.registro_remetido = 1;
            cobranca.remetido = 1;

            cobranca.gerencianet_cobranca_chave = b.chave_gnet ?? '';
            cobranca.transacao_id = b.chave_gnet2 ?? b.chave_juno ?? '';

            cobranca.data_vencimento = b.datavenc;
            cobranca.data_gerado = b.processamento;
            cobranca.mensagem = b.obs;

            cobranca.status = (b.status == 'pago') ? 1 : 0;
            cobranca.data_pagamento = (cobranca.status == 1) ? b.datapag : null;

            if (b.deltitulo == 1) {
                cobranca.status = 2;
                cobranca.data_cancelado = b.datadel;
            }

            cobrancaBoleto = new CobrancasBoleto();
            cobrancaBoleto.id = cobranca.id;
            cobrancaBoleto.linha_digitavel = b.linhadig ?? '';
            cobrancaBoleto.vencimento_contra_apresentacao = 0;
            cobrancaBoleto.setCriarLinhaDigitavel(false);

            cobranca.cobrancas_boleto_id = cobranca.id;

            if (b.tipo == 'mensalidade') {
                cobranca.mensagem = '';
                cobranca.tipo = 1;
                cobranca.valor_mensalidade = b.valor;
                if (b.referencia && (b.referencia.length == 10)) {
                    cobranca.referencia_mensalidade = b.referencia;
                } else {
                    let dataVencimento = b.getDatavenc;
                    let refMensalidade = data.subMesData(dataVencimento);
                    cobranca.referencia_mensalidade = refMensalidade.format('Y-m-01');
                }
                cobranca.proporcional_mensalidade = data.getUltimoDiaMes(cobranca.getReferenciaMensalidade);
                cobranca.referencia_mensalidade_inicio = data.subMesData(b.getDatavenc).format('Y-m-d');
                cobranca.referencia_mensalidade_fim = data.subDiasData(b.getDatavenc, 1).format('Y-m-d');
                cobranca.desconto_antecipacao = 0;
                cobrancaParcela = new CobrancasParcelas();
                cobrancaParcela.cobrancas_id = cobranca.id;
                cobrancaParcela.tipo = 'MENSALIDADE';
                cobrancaParcela.valor = cobranca.valor_mensalidade;
                cobrancaParcela.mensalidade_inicio = cobranca.referencia_mensalidade_inicio;
                cobrancaParcela.mensalidade_fim = cobranca.referencia_mensalidade_fim;
            }

            // // default não usados
            //
            try {
                if (cobrancaBoleto) {
                    cobrancaBoleto.setLogSistema(false);
                    if (!cobrancaBoleto.Save()) {
                        throw 'Não foi possível salvar os dados do boleto';
                    }
                }
                cobranca.setLogSistema(false);
                cobranca.setCadastraMovimentacao(false);
                cobranca.setImportacao(true);
                cobranca.setSetSequencial(false);
                cobranca.setGeraNossoNumero(false);
                if (!cobranca.Insert()) {
                    throw 'Não foi possível salvar o boleto';
                }
                if (cobrancaParcela) {
                    cobrancaParcela.setLogSistema(false);
                    if (!cobrancaParcela.Save()) {
                        throw 'Não foi possível salvar os valores de parcelas da cobrança';
                    }
                }
            } catch (err) {
                if (err.message) {
                    console.log(err.message);
                } else {
                    console.log(err.getMessage());
                    console.log(err.getFile(), err.getLine(), err.getCode());
                    console.log(err.getTraceAsString());
                    // var_dump(err);
                    console.log(`[${pagina}] Erro inserindo: ${cobranca.id} - ${cobrancaBoleto.id}`)
                }
                exit();
            }
        }
        limitStart += offsetInc;
        gc();
        php.gc_collect_cycles();
    }
    // disconnectDb();
    // return actualCharge;
    return true;

}

function normalizaCobrancas() {
    console.log('Normalizando cobrancas...');
    conn.setDb(ModelBase.getConnection('powerjs\\Models\\mkauth'));
    conn.exec('ALTER TABLE sis_lanc CHANGE `referencia` `referencia` VARCHAR(10) NULL DEFAULT NULL');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-01-01") WHERE referencia LIKE "JAN/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-02-01") WHERE referencia LIKE "FEV/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-03-01") WHERE referencia LIKE "MAR/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-04-01") WHERE referencia LIKE "ABR/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-05-01") WHERE referencia LIKE "MAI/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-06-01") WHERE referencia LIKE "JUN/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-07-01") WHERE referencia LIKE "JUL/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-08-01") WHERE referencia LIKE "AGO/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-09-01") WHERE referencia LIKE "SET/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-10-01") WHERE referencia LIKE "OUT/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-11-01") WHERE referencia LIKE "NOV/%"');
    conn.exec('UPDATE sis_lanc SET referencia = CONCAT(SUBSTR(referencia, 5, 4), "-12-01") WHERE referencia LIKE "DEZ/%"');
    conn.exec('ALTER TABLE `sis_lanc` CHANGE `referencia` `referencia` DATE NULL DEFAULT NULL');
    conn.exec('UPDATE sis_lanc set login = trim(BOTH "\r" FROM login)');
    conn.exec('UPDATE sis_lanc SET datavenc = referencia WHERE datavenc IS NULL AND LENGTH(referencia) = 10');
    conn.exec('UPDATE sis_lanc SET datavenc = processamento WHERE datavenc IS NULL');
}

function testefork() {
    sleep(1);
    console.log('thread');
}

export function migra() {
    // normalizaCobrancas();
    console.log('Verificando quantidade de cobrancas a serem migradas...');
    let rowCount = SisLanc.rowCount();
    if (rowCount === false) {
        console.log('Erro ao tentar localizar as cobrancas')
        exit();
    }

    let threads = ThreadManager.getCpuCount();

    // @ts-ignore
    let rowsPerThread = Math.ceil(rowCount / threads);

    console.log(`${rowCount} cobranças encontradas`);

    listaBancos = SisBoleto.toList('id', 'id');

    let pm = new ThreadManager(threads);
    console.log('iniciando childs...');

    let consoleBaseLine = console_tools.getCursorPosition()[0];

    for (let threadNum = 1; threadNum <= threads; threadNum++) {
            pm.startProcess(function (thisProcess: ThreadManager, pagina, linhasPagina, consBaseLine) {
                // o fork precisa de uma nova conexão com o banco de dados
                connectDb();
                migraLote(pagina, linhasPagina, consBaseLine);
            }, threadNum, rowsPerThread, consoleBaseLine);
    }
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log("Esperando todos os childs concluirem...");
    pm.waitChildsFinish();
    // o processo principal precisa restabelecer as conexões com o banco de dados
    // connectDb();
    console_tools.setCursorPosition(consoleBaseLine+(threads*2)+2);
}