import { base as ModelBase, Internetplanos, ClientesHasInternetplanos, DescontosAcrescimos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import { data } from "powerjs/support/helper";
import console_tools from "powerjs/support/console_tools";
import SisAdicional from "../models/SisAdicional";

let bloqueadoStatus = {
    'nao': 1,
    'default' : 0
};
let tipoBloqueio = {
    'man': 0,
    'default' : 1
};
let planosMigrados = Internetplanos.toList('id', 'nome');

export function clear() {
    console.log('Limpando serviços de internet...');
    conn.setDb(ModelBase.getConnection("duobox"));
    try {
        conn.beginTransaction();
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes_has_internetplanos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "INTERNET"');
        conn.exec('TRUNCATE descontos_acrescimos');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado serviços de internet: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabela de serviços de internet truncada');
}

function ip2long(IP: any) {
    let i = 0;
    IP = IP.match( /^([1-9]\d*|0[0-7]*|0x[\da-f]+)(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?$/i );
    if (!IP) { return false; }
    IP[0] = 0;
    for (i = 1; i < 5; i += 1) {
        IP[0] += !!((IP[i] || '').length);
        IP[i] = parseInt(IP[i]) || 0;
    }
    IP.push(256, 256, 256, 256);
    IP[4 + IP[0]] *= Math.pow(256, 4 - IP[0]);
    if (IP[1] >= IP[5] || IP[2] >= IP[6] || IP[3] >= IP[7] || IP[4] >= IP[8]) { return false; }
    return IP[1] * (IP[0] === 1 || 16777216) + IP[2] * (IP[0] <= 2 || 65536) + IP[3] * (IP[0] <= 3 || 256) + IP[4] * 1;
}

export function insereServico(contract) {
    let desconto = null;
    let acrescimo = null;
    let servico = new ClientesHasInternetplanos();

    servico.id = contract.id;
    servico.clientes_id = contract.id ?? contract.clientes_id;
    servico.internetplanos_id = planosMigrados[contract.plano] ?? planosMigrados[Object.keys(planosMigrados)[0]];
    servico.empresas_id = 1;
    let bancoId = parseInt(contract.conta);
    servico.bancos_id = (bancoId !== bancoId) ? 1 : bancoId+1;
    servico.tipo_conexao = contract.tipo.toUpperCase();
    servico.radio_mac = contract.mac ? contract.mac.replace(':', '') : '';

    servico.status = bloqueadoStatus[contract.bloqueado] || bloqueadoStatus['default'];

    servico.bloqueio_automatico = tipoBloqueio[contract.tipobloq] || tipoBloqueio['default'];
    servico.pppoe_login = contract.login;
    servico.pppoe_senha = contract.senha;
    servico.pppoe_tipo_ip = 'DINAMICO';
    servico.pppoe_ip = 0;
    let ipLong = contract.ip ? ip2long(contract.ip) : 0;
    if ((servico.tipo == 'PPPOE') && (ipLong > 0)) {
        servico.pppoe_tipo_ip = 'FIXO';
        servico.pppoe_ip = ipLong;
    }
    servico.hotspot_filtro_ip = 0;
    if ((contract.tipo == 'hotspot') && (ipLong > 0)) {
        servico.hotspot_filtro_ip = ipLong;
    }
    servico.filtro_mac = servico.radio_mac;
    servico.dia_vencimento = contract.venc;
    servico.nota_fiscal_cfop = (contract.cpf_cnpj.length <= 11) ? 5307 : 5303;
    servico.nota_fiscal_tipo_assinante = (contract.cpf_cnpj.length <= 11) ? 3 : 1;
    servico.nota_fiscal_21_informacoes_complementares = '';
    servico.valor_instalacao = 0;
    servico.quantidade_parcelas_instalacao = 0;
    servico.data_instalacao = contract.data_ins;
    servico.prazo_contrato = 12;
    servico.meses_gratuitos = 0;
    servico.ponto_acesso = contract.ssid;

    servico.equipamento_instalado = contract.comodato == 'nao' ? 'COMPRADO' : 'COMODATO';
    servico.data_cadastro = contract.cadastro ? data.convertDataBrToDb(contract.cadastro) : '0000-00-00';

    servico.data_alteracao = servico.data_cadastro;

    servico.motivo_rescisao = '';
    servico.rescisao_usuarios_id = 0;
    servico.cadastro_usuarios_id = 0;
    servico.alteracao_usuarios_id = 0;
    servico.debito_agencia = '';
    servico.debito_id_cliente_empresa = '';
    servico.debito_id_cliente_banco = '';
    servico.ativo = (contract.cli_ativado == 's') ? 1 : 0;
    if (servico.ativo == 0) {
        servico.data_rescisao = contract.data_desativacao ?? '0000-00-00';
    }

    servico.isento = (contract.isento == 'sim') ? 1 : 0;
    servico.desconto_plano_pagamento_antecipado = 1;

    servico.endereco_instalacao_igual_principal = 1;

    servico.dici_scm_tipo_atendimento = 'URBANO';
    servico.dici_scm_tecnologias_id = 13;
    servico.tecnologia = 'h';

    servico.valor_desconto = 0;
    servico.valor_acrescimo = 0;
    if (contract.desconto > 0) {
        desconto = new DescontosAcrescimos();
        desconto.clientes_has_internetplanos_id = servico.id;
        desconto.data_inicial = '0000-00-00';
        desconto.tipo = 'DESCONTO';
        desconto.valor = contract.desconto;
        desconto.tipo_valor = 'VALOR';
        desconto.motivo = '';
        desconto.removido = 0;
        desconto.data_hora_cadastro = servico.data_cadastro;
    }
    if (contract.acrescimo > 0) {
        acrescimo = new DescontosAcrescimos();
        acrescimo.clientes_has_internetplanos_id = servico.id;
        acrescimo.data_inicial = '0000-00-00';
        acrescimo.tipo = 'ACRESCIMO';
        acrescimo.valor = contract.acrescimo;
        acrescimo.tipo_valor = 'VALOR';
        acrescimo.motivo = '';
        acrescimo.removido = 0;
        acrescimo.data_hora_cadastro = servico.data_cadastro;
    }

    servico.radio_login = '';
    servico.radio_senha = '';
    servico.wpa_login = '';
    servico.wpa_senha = '';
    servico.suspensao_motivo = '';
    servico.ip_gateway = 0;
    servico.ip_mascara = 0;
    servico.ip_rede = 0;
    servico.ip_broadcast = 0;
    servico.mikrotik_interfaces_id = 0;
    servico.bloqueio_hora_dias = '';

    try {
        servico.setLogSistema(false);
        servico.Insert();
        if (desconto) {
            desconto.setLogSistema(false);
            desconto.Insert();
        }
        if (acrescimo) {
            acrescimo.setLogSistema(false);
            acrescimo.Insert();
        }
    } catch (e) {
        var_dump(e.getMessage());
        exit();
    }
}

export function migra() {
    console.log('Buscando serviços adicionais para migrar...');
    let adicionais: SisAdicional[] = SisAdicional.all();
    let totalPlans = adicionais.length
    let actualPlan = 0;
    adicionais.forEach((adicional) => {
        let servico = adicional.SisCliente;
        servico.clientes_id = servico.id;
        servico.id = null;
        servico.tipo = adicional.tipo;
        servico.mac = adicional.mac;
        servico.bloqueado = adicional.bloqueado;
        servico.login = adicional.username;
        servico.senha = adicional.senha;
        servico.ip = adicional.ip;
        insereServico(servico);
        console_tools.mostraProgresso(++actualPlan, totalPlans, `Migrando servico ${servico.clientes_id} - ${servico.login}`);
    });

    console.log('');
}
