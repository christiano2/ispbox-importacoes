import SisCliente from "./SisCliente";
import SisPlano from "./SisPlano";
import SisLanc from "./SisLanc";
import SisAdicional from "./SisAdicional";
import SisBoleto from "./SisBoleto";

export const models: Record<string, any> = {
    SisCliente,
    SisPlano,
    SisLanc,
    SisAdicional,
    SisBoleto
};

export function setupModels() {
    for (let model in models) {
        models[model].setupModel();
    }
}
