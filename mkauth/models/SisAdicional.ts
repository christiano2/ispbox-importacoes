import { MKAuthModel } from "./common";
import SisCliente from "./SisCliente";
import SisPlano from "./SisPlano";

export default class SisAdicional extends MKAuthModel {
    static relations: Record<string, any> = {
        belongsTo: {
            SisCliente: {
                class: SisCliente.className(),
                foreignKey: 'login',
                parentKey: 'login'
            },
            Plano: {
                class: SisPlano.className(),
                foreignKey: 'plano',
                parentKey: 'nome'
            }
        }
    }

    static {
        this.initQueue();
    }
}
