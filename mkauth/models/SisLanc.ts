import { MKAuthModel } from "./common";
import ClientesHasInternetplanos from "powerjs/db/models/ClientesHasInternetplanos";

export default class SisLanc extends MKAuthModel {
    public ClientesHasInternetplanos: ClientesHasInternetplanos;
    static relations: Record<string, any> = {
        belongsTo: {
            ClientesHasInternetplanos: {
                class: 'duobox\\Models\\ClientesHasInternetplanos',
                foreignKey: 'login',
                parentKey: 'pppoe_login'
            }
        }
    }

    static {
        this.initQueue();
    }
}
