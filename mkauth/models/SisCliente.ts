import { MKAuthModel } from "./common";
import SisPlano from "./SisPlano";
import SisLanc from "./SisLanc";

export default class SisCliente extends MKAuthModel {
    static relations: Record<string, any> = {
        belongsTo: {
            Plano: {
                class: SisPlano.className(),
                foreignKey: 'plano',
                parentKey: 'nome'
            }
        },
        hasMany: {
            SisLanc: {
                class: SisLanc.className(),
                foreignKey: 'login',
                parentKey: 'login'
            }
        }
    }

    static {
        this.initQueue();
    }
}
