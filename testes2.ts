// // import Clientes from "powerjs/db/models/Clientes";
// import { performance } from "powerjs/perf_hooks/microtime";
// import MunicipiosIbge from "powerjs/db/models/MunicipiosIbge";
// // import Clientes from "powerjs/db/models/Clientes";
//
// // let cliente = new Clientes();
//
// let newObj = 0;
// let iterations = 4000;
// let timeInitMain = performance.now2();
//
// for (let i = 0; i<=iterations; i++) {
//     let timeInit = performance.now2();
//     let cliente = MunicipiosIbge.first('uf_sigla = ? AND municipio_nome = ?', [
//         'PR',
//         'Cascavel'
//     ]);
//     let timeFim = performance.now2();
//     let diff = timeFim - timeInit;
//     newObj += diff;
// }
// let timeFimMain = performance.now2();
//
// console.log(`Média newOjb: ${newObj/iterations}`);
// console.log(`Tempo total: ${timeFimMain-timeInitMain}`);
//

import base from "powerjs/db/models/base";

base.get('SisLanc', {
    belongsTo: {
        ClientesHasInternetplanos: {
            class: 'duobox\\Models\\ClientesHasInternetplanos',
            foreignKey: 'login',
            parentKey: 'pppoe_login'
        }
    }
});

