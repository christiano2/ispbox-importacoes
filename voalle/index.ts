import { dbConnect } from "./common.ts";
import { models, setupModels } from "./models/models.ts";
import { voalle as config } from "../config.json" assert { type: "json" };
import * as DadosClientes from "./dados/Clientes.ts";

// primeiro iniciamos a conexão
export const dbConnection = dbConnect(config.database);

setupModels();

export { models };

export function run() {
    DadosClientes.clear();
}

