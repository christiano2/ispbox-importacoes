import conn from "powerjs/db/conn";
import models from "powerjs/db/models";

const baseNamespace = 'powerjs\\Models\\Voalle';

function dbConnect(database: object) {
    // conectamos no banco de dados
    const psql = conn.newADOConnection(`postgres9://${database.login}:${database.password}@${database.host}/${database.dbname}`);

    // setamos o schema erp cpara busca das tabelas
    if ('schema' in database) {
        psql.Execute(`SET search_path TO ${database.schema}`);
    }

    // setamos que os models criados pelo powerjs devem usar esta conexão
    models.addConnection(baseNamespace, psql);
    return psql;
}

export { dbConnect }
