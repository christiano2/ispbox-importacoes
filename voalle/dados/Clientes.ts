import {base as ModelBase, Clientes, ClientesContatos, MunicipiosIbge} from "powerjs/db/models";
import conn from "powerjs/db/conn";
import console_tools from "powerjs/support/console_tools";
import {numerico} from "powerjs/support/helper";
import * as Tools from "../../lib/tools";
import People from "../models/People";

export function clear() {
    console.log('Limpando dados de clientes...');
    // setamos o conn para usar a conexão padrao do mvc
    conn.setDb(ModelBase.getConnection('duobox'));

    conn.beginTransaction();
    try {
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes');
        conn.exec('TRUNCATE clientes_contatos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "CLIENTE"');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
        console.log('Tabela de clientes truncada');
    } catch(err) {
        conn.rollback();
        console.log(`Erro limpado clientes: ${err.getMessage()}`);
        console.log(err);
        exit();
    }
}

export function migra() {
    // let subs: SisCliente[] = SisCliente.all('', false, { 'limit': 10, 'offset': 0 });
    console.log('Buscando cadastros de clientes');
    let clientes: People[] = People.all('client = ? AND deleted = ?', [true, false]);
    let totalClientes = clientes.length;
    let clienteAtual = 0;

    let ddd = '41';

    const gender = {
        '1': 'M',
        '2': 'F',
        default: 'NI'
    }

    const typeTxId = {
        '1': 'J',
        '2': 'F'
    }

    clientes.forEach((clienteVoalle) => {
        console_tools.mostraProgresso(++clienteAtual, totalClientes, `Migrando cliente ${clienteVoalle.id} - ${clienteVoalle.name}`);
        let cliente = new Clientes();
        let contatos: ClientesContatos[] = [];

        /**
         * campos para entender o significado:
         * situation
         * civil_status
         * state_registration_type
         */



        cliente.id = clienteVoalle.id;
        cliente.nome = clienteVoalle.name;

        cliente.data_nascimento = clienteVoalle.birth_date ?? '0000-00-00';
        // 1 = M
        // 2 = F
        cliente.sexo = gender[clienteVoalle.gender] || gender.default;

        /*
         * civil_status
         * NULL
         * 1
         * 2
         * 3
         * 4
         * 5
         * 6
         */
        cliente.estado_civil = clienteVoalle.civil_status;
        cliente.nome_pai = '';
        cliente.nome_mae = clienteVoalle.parents_name ?? '';
        cliente.inscricao_municipal = '';
        cliente.responsavel = '';
        cliente.exterior = 0;
        cliente.documento_exterior = 0;
        cliente.ruc = '';
        cliente.pais = '';
        cliente.profissao = '';
        cliente.local_trabalho = '';
        cliente.produtor_rural = 0;

        if (clienteVoalle.type_tx_id == 2) {
            cliente.tipo_pessoa = Clientes.TIPO_PESSOA_FISICA;
            cliente.cpf = numerico.apenasNumero(clienteVoalle.tx_id ?? '');
            cliente.rg = clienteVoalle.identity ?? '';
            cliente.produtor_rural = (clienteVoalle.rural_producer ? 1 : 0);
        }
        else {
            cliente.tipo_pessoa = Clientes.TIPO_PESSOA_JURIDICA;
            cliente.cnpj = numerico.apenasNumero(clienteVoalle.tx_id ?? '');
            cliente.nome_fantasia = clienteVoalle.name_2 ?? '';
            /**
             * state_registration_type
             */
            cliente.inscricao_estadual = clienteVoalle.state_registration ?? '';
            cliente.inscricao_municipal = clienteVoalle.municipal_registration ?? '';
            cliente.responsavel = '';
        }


        cliente.email = clienteVoalle.email;

        if (clienteVoalle.email_NFE) {
            let contato = new ClientesContatos();
            contato.clientes_id = cliente.id;
            contato.tipo = 'FINANCEIRO';
            contato.nome = cliente.nome;
            contato.email = clienteVoalle.email_NFE;
            contato.observacoes = '';
            contatos.push(contato);
        }
        /*
         phone
         commercial_phone
         fax_phone
         cell_phone_1
         cell_phone_2

         percebi que vários campos tem valores repetidos
         então montamos uma lista nova com os telefones para evitar repetidos
         */
        function montaListaTelefones(clienteVoalle: People) {
            let phoneFields = [
                'phone',
                'commercial_phone',
                'fax_phone',
                'cell_phone_1',
                'cell_phone_2'
            ]
            let telefones = [];
            phoneFields.forEach((campoTelefone) => {
                let tmpTelefone = Tools.telefone(clienteVoalle[campoTelefone], ddd);
                if (tmpTelefone && telefones.indexOf(tmpTelefone) == -1) {
                    telefones.push(tmpTelefone);
                }
            })
            return telefones;
        }

        let telefones = montaListaTelefones(clienteVoalle);

        cliente.telefone = telefones[0] ?? '';
        cliente.celular = telefones[1] ?? '';

        if (telefones.length > 2) {
            for (let i = 2; i <= telefones.length-1; i++) {
                let contato = new ClientesContatos();
                contato.clientes_id = cliente.id;
                contato.tipo = 'FINANCEIRO';
                contato.nome = cliente.nome;
                contato.telefone = telefones[i];
                contato.observacoes = '';
                contatos.push(contato);
            }
        }

        cliente.cep = Tools.cep(clienteVoalle.postal_code);
        cliente.endereco = clienteVoalle.street;
        cliente.endereco_numero = clienteVoalle.number ?? 0;
        cliente.endereco_complemento = clienteVoalle.address_complement ?? '';
        cliente.endereco_referencia = clienteVoalle.address_reference ?? '';
        cliente.endereco_bairro = clienteVoalle.neighborhood;
        cliente.cidade = clienteVoalle.city;
        cliente.uf = clienteVoalle.state;
        cliente.coordenadas_gps = (clienteVoalle.lat + ' ' + clienteVoalle.lng).trim();
        cliente.cidade_ibge = clienteVoalle.code_city_id;

        if (!cliente.cidade_ibge) {
            let ibge = MunicipiosIbge.first('uf_sigla = ? AND municipio_nome = ?', [
                cliente.uf,
                cliente.cidade
            ]);

            if (ibge) {
                cliente.cidade_ibge = `${ibge.uf_codigo}${ibge.municipio_codigo}`;
            }
        }

        cliente.cobranca_igual = 1;
        cliente.cobranca_endereco_referencia = '';
        cliente.cobranca_cidade_ibge = '';
        cliente.cobranca_pais = '';
        cliente.cobranca_coordenadas_gps = '';

        cliente.cadastro_usuarios_id = 0;
        cliente.alteracao_usuarios_id = 0;

        cliente.documento_bloqueado_observacao = '';

        let dataCadastro = clienteVoalle.getCreated;

        cliente.data_cadastro = dataCadastro.format('Y-m-d');

        let dataAlteracao = clienteVoalle.getModified;
        cliente.data_alteracao = dataAlteracao.format('Y-m-d');

        try {
            cliente.setLogSistema(false);
            cliente.Insert();
            if (contatos.length > 0) {
                contatos.forEach((contato) => {
                    contato.setLogSistema(false);
                    contato.Save();
                });
            }
            let totalContratos = clienteVoalle.Clientecontrato.length;
            if (totalContratos > 0) {
                DadosServicoInternet.insereServicos(clienteVoalle);
            }
        } catch (e) {
            var_dump(e.message ?? e.getMessage());
            exit();
        }
        // chamada para inserir o serviço
    });

    console.log();
}

