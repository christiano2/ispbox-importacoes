import { VoalleModel } from "./common.ts";

export default class FinancialReceivableTitles extends VoalleModel {
	static relations: Record<string, any> = {
		belongsTo: {
			BankAccount: VoalleModel.namespaceClass('BankAccounts')
		}
	}
}
