import People from "./People.ts";
import PeopleAddresses from "./PeopleAddresses.ts";
import Contracts from "./Contracts.ts";
import BankAccounts from "./BankAccounts.ts";
import FinancialReceivableTitles from "./FinancialReceivableTitles.ts";

export const models: Record<string, any> = {
    People,
    PeopleAddresses,
    Contracts,
    BankAccounts,
    FinancialReceivableTitles
};

export function setupModels() {
    for (let model in models) {
        models[model].setupModel();
    }
}
