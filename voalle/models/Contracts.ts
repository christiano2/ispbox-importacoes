import { VoalleModel } from "./common";

export default class Contracts extends VoalleModel {
	static relations: Record<string, any> = {
		belongsTo: {
			Client: VoalleModel.namespaceClass('People')
		}
	}
}

