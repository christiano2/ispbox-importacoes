import { VoalleModel } from "./common";

export default class People extends VoalleModel {
    static relations: Record<string, any> = {
        hasMany: {
            PeopleAddresses: {
                class: VoalleModel.namespaceClass('PeopleAddresses'),
                foreignKey: 'person_id'
            }
        }
    }
    // testes de métodos estáticos
    static primeiroPJ() {
        return this.first('type_tx_id = 1');
    }
    // testes de métodos estáticos
    static primeiroPF() {
        return this.first('type_tx_id = 2');
    }
}
