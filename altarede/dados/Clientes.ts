import { base as ModelBase, Clientes, ClientesContatos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import { Clientes as ClientesAR } from "../models/Clientes";
import { numerico } from "powerjs/support/helper";
import * as DadosServicoInternet from './ServicosInternet';
import * as Tools from "../../lib/tools";
import console_tools from "powerjs/support/console_tools";

export function clear() {
    console.log('Limpando dados de clientes...');
    // setamos o conn para usar a conexão padrao do mvc
    conn.setDb(ModelBase.getConnection('duobox'));

    conn.beginTransaction();
    try {
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes');
        conn.exec('TRUNCATE clientes_contatos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "CLIENTE"');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
        console.log('Tabela de clientes truncada');
    } catch(err) {
        conn.rollback();
        console.log(`Erro limpado clientes: ${err.getMessage()}`);
        console.log(err);
        exit();
    }
}

export function migra() {
    // let subs: SisCliente[] = SisCliente.all('', false, { 'limit': 10, 'offset': 0 });
    console.log('Buscando cadastros de clientes');
    let clientes: ClientesAR[] = ClientesAR.all();
    let totalClientes = clientes.length;
    let clienteAtual = 0;

    let ddd = '75';

    let sexo = {
        M: 'M',
        F: 'F',
        default: 'NI'

    }

    let enderecoRegex = / *(?<numero>\d+)? *(?<complemento>.*)/;

    clientes.forEach((clienteAR) => {
        console_tools.mostraProgresso(++clienteAtual, totalClientes, `Migrando cliente ${clienteAR.codigo} - ${clienteAR.nome}`);
        if (clienteAR.tipo_cliente != '' && clienteAR.nome != '') {
            let cliente = new Clientes();
            let contatos: ClientesContatos[] = [];

            // sub.celular = numerico.apenasNumero(sub.celular ?? '');
            // sub.fone = numerico.apenasNumero(sub.fone ?? '');
            // if (sub.celular.length == 16) {
            //     celular3 = Tools.telefone(sub.celular.substring(8), ddd);
            //     sub.celular = sub.celular.substring(0,8);
            // }
            // sub.fone = Tools.telefone(sub.fone, ddd);
            // sub.celular = Tools.telefone(sub.celular, ddd);
            cliente.id = clienteAR.codigo;
            cliente.nome = clienteAR.nome;

            cliente.data_nascimento = clienteAR.data_nascimento ?? '0000-00-00';
            cliente.sexo = sexo[clienteAR.sexo] ?? sexo.default;

            cliente.estado_civil = 0;
            cliente.nome_pai = clienteAR.pai ?? '';
            cliente.nome_mae = clienteAR.mae ?? '';
            cliente.inscricao_municipal = '';
            cliente.responsavel = '';
            cliente.profissao = '';
            cliente.local_trabalho = '';

            if (clienteAR.tipo_cliente == Clientes.TIPO_PESSOA_FISICA) {
                cliente.tipo_pessoa = Clientes.TIPO_PESSOA_FISICA;
                cliente.cpf = numerico.apenasNumero(clienteAR.cpf ?? '');
                cliente.rg = (clienteAR.identidade ?? '') + (clienteAR.orgao_emissor ?? '');
                cliente.profissao = clienteAR.profissao ?? '';
                cliente.local_trabalho = clienteAR.trabalho ?? '';
            } else {
                cliente.tipo_pessoa = Clientes.TIPO_PESSOA_JURIDICA;
                cliente.cnpj = numerico.apenasNumero(clienteAR.cpf ?? '');
                cliente.nome_fantasia = clienteAR.nome;
                cliente.inscricao_estadual = '';
                cliente.inscricao_municipal = '';
                cliente.responsavel = '';
            }

            cliente.exterior = 0;
            cliente.documento_exterior = 0;
            cliente.ruc = '';
            cliente.pais = '';
            cliente.produtor_rural = 0;
            cliente.email = clienteAR.email1;
            cliente.email2 = clienteAR.email2;
            cliente.telefone = Tools.telefone(clienteAR.telefone_residencial, ddd);
            cliente.celular = Tools.telefone(clienteAR.telefone_celular1, ddd);

            if (clienteAR.telefone_comercial) {
                let contato = new ClientesContatos();
                contato.clientes_id = cliente.id;
                contato.tipo = 'FINANCEIRO';
                contato.nome = cliente.nome;
                contato.telefone = Tools.telefone(clienteAR.telefone_comercial, ddd);
                contato.observacoes = '';
                contatos.push(contato);
            }
            if (clienteAR.telefone_celular2) {
                let contato = new ClientesContatos();
                contato.clientes_id = cliente.id;
                contato.tipo = 'FINANCEIRO';
                contato.nome = cliente.nome;
                contato.telefone = Tools.telefone(clienteAR.telefone_celular2, ddd);
                contato.observacoes = '';
                contatos.push(contato);
            }

            cliente.cep = Tools.cep(clienteAR.cep);
            cliente.endereco = clienteAR.logradouro + ' ' + clienteAR.endereco;

            let enderecoNumero = clienteAR.complemento.match(enderecoRegex);

            cliente.endereco_numero = enderecoNumero.groups.numero ?? 0;
            cliente.endereco_complemento = enderecoNumero.groups.complemento ?? '';
            cliente.endereco_referencia = clienteAR.obs ?? '';
            cliente.endereco_bairro = clienteAR.bairro;
            cliente.cidade = clienteAR.cidade;
            cliente.uf = clienteAR.estado;
            cliente.coordenadas_gps = '';
            cliente.cidade_ibge = clienteAR.cod_cidade;

            cliente.cobranca_igual = 1;
            cliente.cobranca_endereco_referencia = '';
            cliente.cobranca_cidade_ibge = '';
            cliente.cobranca_pais = '';
            cliente.cobranca_coordenadas_gps = '';

            cliente.cadastro_usuarios_id = 0;
            cliente.alteracao_usuarios_id = 0;

            cliente.documento_bloqueado_observacao = '';

            cliente.data_cadastro = clienteAR.data_cadastro;

            cliente.data_alteracao = '0000-00-00';

            try {
                cliente.setLogSistema(false);
                cliente.Insert();
                if (contatos.length > 0) {
                    contatos.forEach((contato) => {
                        contato.setLogSistema(false);
                        contato.Save();
                    });
                }
                let totalContratos = clienteAR.Logins.length;
                if (totalContratos > 0) {
                    DadosServicoInternet.insereServicos(clienteAR);
                }
            } catch (e) {
                console.log(e.params);
                var_dump(e.message ?? e.getMessage());
                let type;
                let params = '';
                e.params.forEach((value, index) => {
                    console.log(index,value);
                    if (index == 0) {
                        type = value.split('');
                    }
                    else {
                        console.log('type', type[index-1]);
                        if (value == null) {
                            params += 'null,';
                        }
                        else if (type[index-1] == 's') {
                            params += '"'+value+'",';
                        }
                        else {
                            params += value+',';
                        }
                    }
                })
                console.log(params);
                exit();
            }
        }
        // chamada para inserir o serviço
    });

    console.log();
}
