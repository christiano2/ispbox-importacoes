import { AltaRedeModel } from "./AltaRedeModel";
import Planos from "./Planos";

class Logins extends AltaRedeModel {
    static relations: Record<string, any> = {
        belongsTo: {
            Plano: {
                class: Planos.className(),
                foreignKey: 'cod_plano',
                parentKey: 'cod_plano'
            }
        },
    //     hasMany: {
    //         SisLanc: {
    //             class: Financeiro.className(),
    //             foreignKey: 'login',
    //             parentKey: 'login'
    //         }
    //     }
    }

    static {
        this.initQueue();
    }
}

export default Logins;
export { Logins };
