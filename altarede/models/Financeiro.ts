import { AltaRedeModel } from "./AltaRedeModel";

class Financeiro extends AltaRedeModel {
    // static relations: Record<string, any> = {
    //     belongsTo: {
    //         ClientesHasInternetplanos: {
    //             class: 'duobox\\Models\\ClientesHasInternetplanos',
    //             foreignKey: 'login',
    //             parentKey: 'pppoe_login'
    //         }
    //     }
    // }

    getClienteId() {
        let codigos = this.cod_cliente.split('.');
        return codigos[1];
    }

    static {
        this.initQueue();
    }
}

export default Financeiro;
export { Financeiro };