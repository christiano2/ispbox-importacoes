import { Model } from "../../lib/model";

class AltaRedeModel extends Model {
    public static namespace = 'AltaRede';
}

export default AltaRedeModel;
export { AltaRedeModel };
