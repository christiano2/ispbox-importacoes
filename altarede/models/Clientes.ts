import { AltaRedeModel } from "./AltaRedeModel";
import Logins from "./Logins";
import Financeiro from "./Financeiro";

class Clientes extends AltaRedeModel {
    // Logins: Logins[];
    // Financeiro: Financeiro[];
    static relations: Record<string, any> = {
    //     belongsTo: {
    //         Plano: {
    //             class: Planos.className(),
    //             foreignKey: 'plano',
    //             parentKey: 'nome'
    //         }
    //     },
        hasMany: {
            Logins: {
                class: Logins.className(),
                foreignKey: 'cod_cliente',
                parentKey: 'cod_cliente'
            },
            Financeiro: {
                class: Financeiro.className(),
                foreignKey: 'cod_cliente',
                parentKey: 'cod_cliente'
            }
        }
    }

    static {
        this.initQueue();
    }
}

export default Clientes;
export { Clientes };
