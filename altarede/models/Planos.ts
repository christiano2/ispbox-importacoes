import { AltaRedeModel } from "./AltaRedeModel";

class Planos extends AltaRedeModel {
    static relations: Record<string, any> = {
    }

    static {
        this.initQueue();
    }
}

export default Planos;
export { Planos };