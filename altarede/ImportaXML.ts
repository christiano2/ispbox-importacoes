import { base as ModelBase } from "powerjs/db/models";
import { dbConnect, baseNamespace } from "./common";
import TableCreate from 'powerjs/migracoes/TableCreate';
import { opendir, readdir, str_ends_with, mb_strtolower, str_replace } from 'powerjs/php/functions';
import { DOMDocument } from 'powerjs/xml/dom';
import { altarede as config } from "../config.json";
import conn from "powerjs/db/conn";

export default function processa(dadosPath)
{
    let dbConnection = ModelBase.getConnection(baseNamespace);
    let analyzeTable = new TableCreate();
    analyzeTable.setDb(dbConnection);
    analyzeTable.setIndexCallback(function(tableName: string, colName: string, colType: string) {
        return (colName.substring(0,4) == 'cod_');
    });

    analyzeTable.setPrimaryKeyCallback(function(tableName: string, colName: string, colType: string) {
        if (tableName == 'os' && colName == 'id_os') {
            return true;
        }
        if (tableName == 'planos' && colName == 'cod_plano') {
            return true;
        }
        if (tableName == 'tickets' && colName == 'cod_atendimento') {
            return true;
        }
        if (tableName == 'financeiro' && colName == 'cod_areceber') {
            return true;
        }
        if (colType == TableCreate.BIGINT || colType == TableCreate.INT) {
            if (colName == 'codigo') {
                return true;
            }
        }
        return false;
    });

    let dbName = config.database.dbname;
    let create = null;
    let options = null;
    let sql_data = [];

    let dadosDir = opendir(dadosPath);
    let entry;

    const XML_ELEMENT_NODE = 1;

    while (false !== (entry = readdir(dadosDir))) {
        if (str_ends_with(entry, '.xls') || str_ends_with(entry, '.xml')) {
            console.log(`Processando ${entry}`);

            let tableName = mb_strtolower(str_replace(['.xls', '.xml'], ['',''], entry));

            let xml = new DOMDocument();
            xml.load(dadosPath + entry);
            let tabela = xml.getElementsByTagName('Worksheet').item(0);
            let colNames = null;
            let rows = [];
            let tableNodeCount = tabela.childNodes.count();
            let tabelaCKey = 0
            for (; tabelaCKey < tableNodeCount; tabelaCKey++) {
                let ssTable = tabela.childNodes.item(tabelaCKey);
                if ((ssTable.nodeType == XML_ELEMENT_NODE) && (ssTable.nodeName == 'ss:Table')) {
                    let ssTableNodeCount = ssTable.childNodes.count();
                    let ssTableCKey = 0
                    for (; ssTableCKey < ssTableNodeCount; ssTableCKey++) {
                        let ssRow = ssTable.childNodes.item(ssTableCKey);
                        if ((ssRow.nodeType == XML_ELEMENT_NODE) && (ssRow.nodeName == 'ss:Row')) {
                            let row = [];
                            let ssRowNodeCount = ssRow.childNodes.count();
                            let ssRowCKey = 0
                            for (; ssRowCKey < ssRowNodeCount; ssRowCKey++) {
                                let ssCol = ssRow.childNodes.item(ssRowCKey);
                                if ((ssCol.nodeType == XML_ELEMENT_NODE) && (ssCol.nodeName == 'ss:Cell')) {
                                    row.push(ssCol.nodeValue);
                                }
                            }

                            if (!colNames) {
                                colNames = row.map(n => mb_strtolower(n))
                            }
                            else {
                                // let linha = rows.length;
                                // console.log(`pushing row ${linha}`);
                                console.log(`pushing row ${rows.length}`);
                                // rows[linha] = row;
                                rows.push(row);
                            }
                        }
                        php.gc_collect_cycles();
                    }
                }
            }

            colNames.forEach((colName, colIndex) => {
                if (colNames.slice(0,colIndex).indexOf(colName) >= 0) {
                    colNames[colIndex] += '_2';
                }

            })
            let rowCount = rows.length;

            if (rowCount == 0) {
                console.log('Arquivo sem conteudo');
                continue;
            }

            console.log('criando sql');

            let tables = [];
            tables.push([
                TableCreate.TBL_NAME = tableName,
                TableCreate.COL_NAMES = colNames,
                TableCreate.ROWS = rows
            ]);
            let analyses = [];

            analyses.push(analyzeTable.analyzeTable(tables[0]));

            try {
                analyzeTable.buildSql(dbName, tables, analyses, create, options, sql_data);
                conn.setDb(dbConnection);
                let result = conn.exec(`SELECT COUNT(*) AS total
                                        FROM ${dbName}.${tableName}`);
                let rowsInserted = result.fields('total');
                console.log(`Registros lidos/inseridos: ${rowCount}/${rowsInserted}`);
            } catch (e) {
                console.log(e, e.toString());
                exit();
            }
        }

    }
}