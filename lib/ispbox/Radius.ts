import conn from "powerjs/db/conn";
import base from "powerjs/db/models/base";
import { Internetplanos, RadiusGroupreply, RadiusHotspotGroupreply } from "powerjs/db/models";

export function cria()
{
    console.log('Limpando radius_groupreply e radius_groupcheck');
    conn.setDb(base.getConnection('duobox'));
    conn.exec('TRUNCATE TABLE radius_check');
    conn.exec('TRUNCATE TABLE radius_reply');
    conn.exec('TRUNCATE TABLE radius_usergroup');
    conn.exec('DELETE FROM radius_groupreply WHERE (GroupName != "bloqueado")');
    conn.exec('DELETE FROM radius_groupcheck WHERE (GroupName NOT IN ("bloqueado","authmac","suspenso"))');
    conn.exec('INSERT INTO clientes_central_assinante (`tipo_login`, `clientes_has_internetplanos_id`, `login`, `senha`,\n' +
        '                                                  `senha_limpa`)\n' +
        'SELECT \'SERVICO_INTERNET\' AS tipo_login, ci.id, ci.pppoe_login, \'asdasd\', \'\' AS senha_limpa\n' +
        'FROM clientes_has_internetplanos AS ci\n' +
        'WHERE ativo = 1');
    conn.exec('INSERT INTO radius_usergroup (UserName, GroupName, priority)\n' +
        'SELECT pppoe_login, internetplanos_id, \'2\'\n' +
        'FROM clientes_has_internetplanos\n' +
        'WHERE tipo_conexao = \'PPPOE\'\n' +
        '  AND ativo = 1');
    conn.exec('INSERT INTO radius_usergroup (UserName, GroupName, priority)\n' +
        'SELECT pppoe_login, \'bloqueado\', \'1\'\n' +
        'FROM clientes_has_internetplanos\n' +
        'WHERE tipo_conexao = \'PPPOE\'\n' +
        '  AND ativo = 1\n' +
        '  AND status = 0');

    conn.exec('INSERT INTO radius_check (UserName, Attribute, op, Value)\n' +
        'SELECT pppoe_login, \'Password\', \':=\', pppoe_senha\n' +
        'FROM clientes_has_internetplanos\n' +
        'WHERE tipo_conexao = \'PPPOE\'\n' +
        '  AND ativo = 1');
    conn.exec('INSERT INTO radius_check (UserName, Attribute, op, Value)\n' +
        'SELECT pppoe_login,\n' +
        '       \'Calling-Station-Id\',\n' +
        '       \'==\',\n' +
        '       CONCAT(SUBSTR(filtro_mac, 1, 2), \':\', SUBSTR(filtro_mac, 3, 2), \':\', SUBSTR(filtro_mac, 5, 2), \':\',\n' +
        '              SUBSTR(filtro_mac, 7, 2), \':\', SUBSTR(filtro_mac, 9, 2), \':\', SUBSTR(filtro_mac, 11, 2))\n' +
        'FROM clientes_has_internetplanos\n' +
        'WHERE tipo_conexao = \'PPPOE\'\n' +
        '  AND ativo = 1\n' +
        '  AND filtro_mac != \'\'');

    conn.exec('INSERT INTO radius_reply (UserName, Attribute, op, Value)\n' +
        'SELECT pppoe_login, \'Framed-IP-Address\', \':=\', INET_NTOA(pppoe_ip)\n' +
        'FROM clientes_has_internetplanos\n' +
        'WHERE tipo_conexao = \'PPPOE\'\n' +
        '  AND ativo = 1\n' +
        '  AND status = 1\n' +
        '  AND pppoe_ip > 0;');

    console.log('Inserindo novos atributos dos grupos');
    let planos: Internetplanos[] = Internetplanos.all();
    for (let planoIndex in planos) {
        console.log(`Plano ${planos[planoIndex].id} - ${planos[planoIndex].nome}`);
        Mikrotik(planos[planoIndex]);
        UbiquitiEdgerouter(planos[planoIndex]);
    }
}

function UbiquitiEdgerouter(plano: Internetplanos)
{
    let downGroupReply = new RadiusGroupreply();
    downGroupReply.vendor = 'UBIQUITI_EDGEROUTER';
    downGroupReply.GroupName = plano.id;
    downGroupReply.op = '=';

    let upGroupReply = Object.assign(new RadiusGroupreply(), downGroupReply);
    let interimGrouReply = Object.assign(new RadiusGroupreply(), downGroupReply);

    downGroupReply.Attribute = 'WISPr-Bandwidth-Max-Up';
    downGroupReply.Value = plano.velocidade_up * 1024;

    upGroupReply.Attribute = 'WISPr-Bandwidth-Max-Down';
    upGroupReply.Value = plano.velocidade_down * 1024;

    downGroupReply.Save();
    upGroupReply.Save();

    interimGrouReply.Attribute = 'Acct-Interim-Interval';
    interimGrouReply.Value = 300;
    interimGrouReply.Save();
}

function Mikrotik(plano: Internetplanos)
{
    let groupReply = new RadiusGroupreply();
    groupReply.vendor = 'MIKROTIK';
    groupReply.GroupName = plano.id;
    groupReply.Attribute = 'Mikrotik-Rate-Limit';
    groupReply.op = ':=';
    groupReply.Value = php.sprintf('%sk/%sk %sk/%sk %sk/%sk %s/%s %s %sk/%sk',
        plano.velocidade_up, plano.velocidade_down, plano.burst_up, plano.burst_down,
        plano.burst_up_threshold, plano.burst_down_threshold, plano.burst_up_time, plano.burst_down_time,
        plano.prioridade, plano.velocidade_up_garantia, plano.velocidade_down_garantia);

    let hsGroupReply = new RadiusHotspotGroupreply();
    hsGroupReply.setDataArray(groupReply.toArray());

    groupReply.Save();
    hsGroupReply.Save();
}