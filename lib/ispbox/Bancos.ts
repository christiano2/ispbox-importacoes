import {Bancos, Cobrancas} from "powerjs/db/models";

export function atualizaSequenciais() {
    let bancos: Bancos[] = Bancos.all();
    bancos.forEach((banco) => {
        banco.criarSequencial();
        let cobranca = Cobrancas.first('bancos_id = ? ORDER BY sequencial DESC', [banco.id]);
        let sequencial = 1;
        if (cobranca && cobranca.sequencial) {
            sequencial = parseInt(cobranca.sequencial);
            sequencial = (sequencial !== sequencial) ? 1 : sequencial+1;
        }
        banco.alteraSequencial(sequencial);
    });
}
