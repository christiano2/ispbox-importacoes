import { numerico } from "powerjs/support/helper";

export function telefone(str: string, ddd: string = '')
{
    str = numerico.apenasNumero(str);
    if (str.length < 8) {
        return '';
    }
    if ((str.length == 8) || ((str.length == 9) && (str.substring(0,1) == '9'))) {
        str = ddd + str;
    }
    let regexDDDZeroRules = /00(?<telefone>9\d{7,8}|\d{8})/;
    let regexDDDZero = new RegExp(regexDDDZeroRules);
    if (regexDDDZero.test(str)) {
        let dddZero = str.match(regexDDDZeroRules);
        str = ddd + dddZero.groups.telefone;
    }
    let regTel = new RegExp('/[1-9][1-9]9\d{7}/');
    if ((str.length == 10) && regTel.test(str)) {
        str = str.substring(0,2) + '9' + str.substring(2);
    }
    return str;
}

export function cep(str: string)
{
    let cep = numerico.apenasNumero(str);
    return cep.substring(0,5) + '-' + cep.substring(5);
}

export function ip2long(IP: any) {
    let i = 0;
    IP = IP.match( /^([1-9]\d*|0[0-7]*|0x[\da-f]+)(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?$/i );
    if (!IP) { return false; }
    IP[0] = 0;
    for (i = 1; i < 5; i += 1) {
        IP[0] += !!((IP[i] || '').length);
        IP[i] = parseInt(IP[i]) || 0;
    }
    IP.push(256, 256, 256, 256);
    IP[4 + IP[0]] *= Math.pow(256, 4 - IP[0]);
    if (IP[1] >= IP[5] || IP[2] >= IP[6] || IP[3] >= IP[7] || IP[4] >= IP[8]) { return false; }
    return IP[1] * (IP[0] === 1 || 16777216) + IP[2] * (IP[0] <= 2 || 65536) + IP[3] * (IP[0] <= 3 || 256) + IP[4] * 1;
}