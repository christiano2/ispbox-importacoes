import models from "powerjs/db/models";

/**
 * wrap class para instanciar novos models
 */
export class Model extends models.base {
    public static namespacePrefix: string = 'powerjs\\Models\\';
    public static namespace: string = '';
    public static relations: Record<string, any> = {};
    public static table: string|false = false;
    protected static classConstructor: any;
    protected static initQueueModels = {};

    public static namespaceClass(className: string): string {
        let prefix = this.namespacePrefix;
        if (this.namespace != '') {
            prefix += this.namespace + '\\';
        }
        return prefix + className;
    }

    public static className() {
        let modelName = this.prototype.constructor.name;
        if (modelName != 'Model') {
            if (this.namespace != '') {
                modelName = `${this.namespace}\\${modelName}`;
            }
            return this.namespacePrefix + modelName;
        }
        return null;
    }

    public static initQueue() {
        let modelName = this.prototype.constructor.name;
        if (modelName != 'Model') {
            Model.initQueueModels[modelName] = this;
        }
    }

    public static initModels() {
        for (let modelIdx in Model.initQueueModels) {
            Model.initQueueModels[modelIdx].setupModel();
        }
    }

    public static setupModel() {
        let modelName = this.prototype.constructor.name;
        if (modelName != 'Model') {
            if (this.namespace != '') {
                modelName = `${this.namespace}\\${modelName}`;
            }
            let methods = {};
            Reflect.ownKeys(this.prototype).forEach((methodName) => {
                if (methodName != 'constructor') {
                    methods[methodName] = Reflect.get(this.prototype, methodName);
                }
            });
            this.classConstructor = models.get(modelName, {
                relations: this.relations,
                methods: methods,
                table: this.table
            });
            Object.setPrototypeOf(this, this.classConstructor);
        }
    }
}