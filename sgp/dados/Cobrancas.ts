import { base as ModelBase, Cobrancas, CobrancasBoleto, CobrancasParcelas, Bancos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import { data } from "powerjs/support/helper";
import console_tools from "powerjs/support/console_tools";
import codigobarra from "powerjs/cobrancas/boletos/codigobarra";
import ThreadManager from "powerjs/ThreadManager";
import { dbConnect, baseNamespace } from "../common"
import { sgp as config } from "../../config.json";
import FinanceiroTitulo from "../models/FinanceiroTitulo";
// import FinanceiroPortador from "../models/FinanceiroPortador";

let listaBancos: any;

export function clear() {
    conn.setDb(ModelBase.getConnection("duobox"));
    console.log('Limpando cobrancas...');
    try {
        conn.beginTransaction();
        conn.disableFk();
        conn.exec('TRUNCATE cobrancas');
        conn.exec('TRUNCATE cobrancas_boleto');
        conn.exec('TRUNCATE cobrancas_parcelas');
        conn.exec('DELETE FROM contas_movimentacao WHERE documento_tipo = "BOLETO"');
        conn.enableFk();
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado cobrancas: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabelas de cobrancas truncadas');
}


function connectDb()
{
    conn.createNewConnection();
    dbConnect(config.database);
}

function disconnectDb()
{
    let connMkauth = ModelBase.getConnection('duobox');
    let connIspbox = ModelBase.getConnection(baseNamespace);

    connMkauth.Disconnect();
    connIspbox.Disconnect();
}

function migraLote(pagina, linhasPagina, consoleBaseLine) {
    connectDb();
    pagina = parseInt(pagina)-1;
    let limitStart = pagina * linhasPagina;
    let limitEnd = (pagina+1) * linhasPagina;
    let offsetInc = 500;
    let linhaConsole = parseInt(consoleBaseLine) + (pagina * 2) + 1;
    conn.setDb(ModelBase.getConnection(baseNamespace));
    let first = conn.first(`SELECT COUNT(*) AS total
                                            FROM (SELECT id FROM financeiro_titulo LIMIT ${linhasPagina} OFFSET ${limitStart}) AS a`);

    let rowTotal = parseInt(first.total);
    let rowCount = 0;

    let statusTitulo = {
        1: Cobrancas.STATUS_ABERTO, // em aberto
        2: Cobrancas.STATUS_PAGO, // pago
        3: Cobrancas.STATUS_CANCELADO // cancelado
    }

    while (limitStart < limitEnd) {
        // verificamos se estamos no final do lote e ajustamos o offsetInc para não pegar registros da página seguinte
        if ((limitStart+offsetInc) > limitEnd) {
            offsetInc -= limitStart+offsetInc-limitEnd;
        }

        let lancamentos: FinanceiroTitulo[];
        try {
            lancamentos = FinanceiroTitulo.all(`1=1 ORDER BY id ASC LIMIT ${offsetInc} OFFSET ${limitStart}`);
        } catch (e) {
            var_dump(e);
            exit();
        }

        lancamentos.forEach((b) => {
            rowCount++;

            console_tools.mostraProgresso(rowCount, rowTotal, `Titulo ID: ${b.id}`, linhaConsole);
            let cobrancaBoleto = null;
            let cobrancaParcela = null;
            let cobranca = new Cobrancas();
            cobranca.id = b.id;
            cobranca.clientes_id = b.cliente_id;
            // existem boletos vinculados com contas que não existem mais
            // então se o boleto está vinculado com uma conta inexistente
            // jogamos para a conta 1 do ispbox que é a cobrança em carteira
            cobranca.bancos_id = parseInt(b.portador_id) + 1;
            cobranca.origem_tipo = b.cobranca_id ? 'INTERNET' : 'CLIENTE';
            cobranca.origem_id = b.cobranca_id ? b.Cobranca.Clientecontrato.id : b.cliente_id;

            cobranca.valor = b.valor;
            cobranca.status = statusTitulo[b.status];
            cobranca.data_pagamento = (cobranca.status == Cobrancas.STATUS_PAGO) ? b.data_pagamento : null;

            cobranca.valor_pago = (cobranca.status == Cobrancas.STATUS_PAGO) ? parseFloat(b.valorpago) : 0;

            cobranca.valor_mensalidade = 0;

            cobranca.metodo_cobranca = Cobrancas.METODO_BOLETO;

            cobranca.nossonumero = b.nosso_numero_f;
            cobranca.sequencial = b.nosso_numero;
            //
            if (b.linha_digitavel) {
                let parse = codigobarra.parseBancoLinhaDigitavel(b.linha_digitavel);
                cobranca.nossonumero = parse.getNossoNumero() + parse.getDvNossoNumero();
                cobranca.sequencial = parse.getSequencial();
            }

            cobranca.referencia_mensalidade = null;
            cobranca.referencia_mensalidade_inicio = null;
            cobranca.referencia_mensalidade_fim = null;
            cobranca.tipo = 1; // ajustar
            cobranca.registrado = 1;
            cobranca.registro_remetido = 1;
            cobranca.remetido = 1;

            // cobranca.gerencianet_cobranca_chave = b.chave_gnet ?? '';
            // cobranca.transacao_id = b.chave_gnet2 ?? b.chave_juno ?? '';

            cobranca.data_vencimento = b.data_vencimento;
            cobranca.data_gerado = b.data_documento;
            cobranca.mensagem = '';
            if (!b.demonstrativo.includes('Referente ao Acesso')) {
                cobranca.mensagem = b.demonstrativo;
            }


            if (cobranca.status == Cobrancas.STATUS_CANCELADO) {
                cobranca.data_cancelado = b.data_cancela;
                cobranca.motivo_cancelado = b.motivocancela;
            }

            cobrancaBoleto = new CobrancasBoleto();
            cobrancaBoleto.id = cobranca.id;
            cobrancaBoleto.linha_digitavel = b.linha_digitavel ?? '';
            cobrancaBoleto.vencimento_contra_apresentacao = 0;

            cobranca.cobrancas_boleto_id = cobranca.id;

            if (cobranca.tipo == Cobrancas.TIPO_MENSALIDADE) {
                cobranca.mensagem = '';
                // cobranca.tipo = 1;
                cobranca.valor_mensalidade = b.valor;

                let dataVencimento = b.getDataVencimento;
                let refMensalidade = data.subMesData(dataVencimento);
                cobranca.referencia_mensalidade = refMensalidade.format('Y-m-01');

                cobranca.proporcional_mensalidade = data.getUltimoDiaMes(cobranca.getReferenciaMensalidade);
                cobranca.referencia_mensalidade_inicio = data.subMesData(dataVencimento).format('Y-m-d');
                cobranca.referencia_mensalidade_fim = data.subDiasData(dataVencimento, 1).format('Y-m-d');
                cobranca.desconto_antecipacao = 0;
                cobrancaParcela = new CobrancasParcelas();
                cobrancaParcela.cobrancas_id = cobranca.id;
                cobrancaParcela.tipo = 'MENSALIDADE';
                cobrancaParcela.valor = cobranca.valor_mensalidade;
                cobrancaParcela.mensalidade_inicio = cobranca.referencia_mensalidade_inicio;
                cobrancaParcela.mensalidade_fim = cobranca.referencia_mensalidade_fim;
            }

            // // default não usados
            //
            try {
                if (cobrancaBoleto) {
                    cobrancaBoleto.setCriarLinhaDigitavel(false);
                    cobrancaBoleto.setLogSistema(false);
                    if (!cobrancaBoleto.Save()) {
                        throw 'Não foi possível salvar os dados do boleto';
                    }
                }
                cobranca.setLogSistema(false);
                cobranca.setCadastraMovimentacao(false);
                cobranca.setImportacao(true);
                cobranca.setSetSequencial(false);
                cobranca.setGeraNossoNumero(false);
                if (!cobranca.Insert()) {
                    throw 'Não foi possível salvar o boleto';
                }
                if (cobrancaParcela) {
                    cobrancaParcela.setLogSistema(false);
                    if (!cobrancaParcela.Save()) {
                        throw 'Não foi possível salvar os valores de parcelas da cobrança';
                    }
                }
            } catch (err) {
                if (err.message) {
                    console.log(err.message);
                } else {
                    console.log(err.getMessage());
                    console.log(err.getFile(), err.getLine(), err.getCode());
                    console.log(err.getTraceAsString());
                    // var_dump(err);
                    console.log(`[${pagina}] Erro inserindo: ${cobranca.id} - ${cobrancaBoleto.id}`)
                }
                exit();
            }

        });
        limitStart += offsetInc;
        gc();
        php.gc_collect_cycles();


    }
    // disconnectDb();
    // return actualCharge;
    return true;

}

export function migra() {
    console.log('Verificando quantidade de cobrancas a serem migradas...');
    let rowCount = FinanceiroTitulo.rowCount();
    if (rowCount === false) {
        console.log('Erro ao tentar localizar as cobrancas')
        exit();
    }

    let threads = ThreadManager.getCpuCount();

    // @ts-ignore
    let rowsPerThread = Math.ceil(rowCount / threads);

    console.log(`${rowCount} cobranças encontradas`);

    // listaBancos = FinanceiroPortador.toList('id', 'id');

    let pm = new ThreadManager(threads);
    console.log('iniciando childs...');

    let consoleBaseLine = console_tools.getCursorPosition()[0];

    for (let threadNum = 1; threadNum <= threads; threadNum++) {
            pm.startProcess(function (thisProcess: ThreadManager, pagina, linhasPagina, consBaseLine) {
                // o fork precisa de uma nova conexão com o banco de dados
                connectDb();
                migraLote(pagina, linhasPagina, consBaseLine);
            }, threadNum, rowsPerThread, consoleBaseLine);
    }
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log("Esperando todos os childs concluirem...");
    pm.waitChildsFinish();
    // o processo principal precisa restabelecer as conexões com o banco de dados
    // connectDb();
    console_tools.setCursorPosition(consoleBaseLine+(threads*2)+2);
}