import {
    base as ModelBase,
    // Internetplanos,
    ClientesHasInternetplanos,
    DescontosAcrescimos,
    MunicipiosIbge
} from "powerjs/db/models";
import conn from "powerjs/db/conn";
import AdmcoreCliente from "../models/AdmcoreCliente";
import * as Tools from "../../lib/tools";
import {CONTRATO_STATUS} from "../models/AdmcoreClientecontratostatus";

export function clear() {
    console.log('Limpando serviços de internet...');
    conn.setDb(ModelBase.getConnection("duobox"));
    try {
        conn.beginTransaction();
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes_has_internetplanos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "INTERNET"');
        conn.exec('TRUNCATE descontos_acrescimos');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado serviços de internet: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabela de serviços de internet truncada');
}

const tipoConexao = {
    ppp: 'PPPOE',
    default: 'PPPOE'
};

const converteStatus = {
    // CONTRATO_STATUS.STATUS_LIBERADO= 1,
    '1': 1,
    // CONTRATO_STATUS.STATUS_CANCELADO= 3,
    '3': 0,
    // CONTRATO_STATUS.STATUS_BLOQUEADO= 4,
    '4': 0,
    // CONTRATO_STATUS.STATUS_SUSPENSAO_PARCIAL = 7
    '7': 3,
    default: 1
}




export function insereServicos(clienteSgp: AdmcoreCliente) {

    clienteSgp.Clientecontrato.forEach((contrato) => {
        let desconto = null;
        // let acrescimo = null;
        let servico = new ClientesHasInternetplanos();

        servico.id = contrato.id;
        servico.clientes_id = contrato.cliente_id;
        servico.internetplanos_id = contrato.ServicoInternet.planointernet_id;
        servico.empresas_id = 1;
        let bancoId = parseInt(contrato.Cobranca.portador_id);
        servico.bancos_id = (bancoId !== bancoId) ? 1 : bancoId + 1;
        servico.tipo_conexao = tipoConexao[contrato.ServicoInternet.tipoconexao] || tipoConexao.default;
        servico.radio_mac = contrato.mac ? contrato.mac.replace(':', '') : '';

        servico.status = converteStatus[contrato.Status.status] || converteStatus.default;

        servico.bloqueio_automatico = 1;
        servico.pppoe_login = contrato.ServicoInternet.login;
        servico.pppoe_senha = contrato.ServicoInternet.login_password_plain;
        servico.pppoe_tipo_ip = 'DINAMICO';
        servico.pppoe_ip = 0;
        let ipLong = contrato.ServicoInternet.ip ? Tools.ip2long(contrato.ServicoInternet.ip) : 0;
        if ((servico.tipo == 'PPPOE') && (ipLong > 0)) {
            servico.pppoe_tipo_ip = 'FIXO';
            servico.pppoe_ip = ipLong;
        }
        servico.hotspot_filtro_ip = 0;
        if ((contrato.tipo == 'hotspot') && (ipLong > 0)) {
            servico.hotspot_filtro_ip = ipLong;
        }
        servico.filtro_mac = servico.radio_mac;
        servico.dia_vencimento = contrato.Cobranca.Vencimento.dia;
        servico.nota_fiscal_cfop = contrato.Cobranca.notafiscal_cfop_id ? contrato.Cobranca.NotafiscalCfop.cfop : null;
        if (!servico.nota_fiscal_cfop) {
            servico.nota_fiscal_cfop = (clienteSgp.Pessoa.tipopessoa == 'F') ? 5307 : 5303;
        }
        servico.nota_fiscal_tipo_assinante = (clienteSgp.Pessoa.tipopessoa == 'F') ? 3 : 1;
        servico.nota_fiscal_21_informacoes_complementares = '';
        servico.valor_instalacao = 0;
        servico.quantidade_parcelas_instalacao = 0;
        servico.data_instalacao = contrato.data_inicio;
        servico.prazo_contrato = 12;
        servico.meses_gratuitos = 0;
        servico.ponto_acesso = '';

        servico.equipamento_instalado = 'COMODATO';
        let dataCadastro = contrato.getDataCadastro;
        servico.data_cadastro = dataCadastro.format('Y-m-d');

        let dataAlteracao = contrato.getDataAlteracao;
        servico.data_alteracao = dataAlteracao.format('Y-m-d');

        servico.motivo_rescisao = '';
        servico.rescisao_usuarios_id = 0;
        servico.cadastro_usuarios_id = 0;
        servico.alteracao_usuarios_id = 0;
        servico.debito_agencia = '';
        servico.debito_id_cliente_empresa = '';
        servico.debito_id_cliente_banco = '';
        servico.ativo = (contrato.Status.status == CONTRATO_STATUS.STATUS_CANCELADO) ? 0 : 1;
        if (servico.ativo == 0) {
            servico.data_rescisao = contrato.Status.data_cadastro;
        }


        // servico.isento = contrato.Cobranca.permuta ? 1 : 0;
        contrato.Cobranca.isento = parseFloat(contrato.Cobranca.isento);
        servico.isento = ((contrato.Cobranca.permuta == 'true') || (contrato.Cobranca.isento == 100.0)) ? 1 : 0;
        servico.desconto_plano_pagamento_antecipado = 1;

        servico.endereco_instalacao_igual_principal = (clienteSgp.endereco_id == contrato.ServicoInternet.endereco_id) ? 1 : 0;
        if (servico.endereco_instalacao_igual_principal == 0) {
            let endereco = contrato.ServicoInternet.Endereco;
            servico.instalacao_cep = Tools.cep(endereco.cep);
            servico.instalacao_endereco = endereco.logradouro;
            servico.instalacao_endereco_numero = endereco.numero ?? 0;
            servico.instalacao_endereco_complemento = endereco.complemento ?? '';
            servico.instalacao_endereco_referencia = endereco.pontoreferencia ?? '';
            servico.instalacao_endereco_bairro = endereco.bairro;
            servico.instalacao_cidade = endereco.cidade;
            servico.instalacao_uf = endereco.uf;
            servico.instalacao_coordenadas_gps = '';
            servico.instalacao_cidade_ibge = '';

            let ibge = MunicipiosIbge.first('uf_sigla = ? AND municipio_nome = ?', [
                servico.instalacao_uf,
                servico.instalacao_cidade
            ]);

            if (ibge) {
                servico.instalacao_cidade_ibge = `${ibge.uf_codigo}${ibge.municipio_codigo}`;
            }
        }

        servico.endereco_cobranca_igual_principal = (clienteSgp.endereco_id == contrato.Cobranca.endereco_id) ? 1 : 0;
        if (servico.endereco_cobranca_igual_principal == 0) {
            let endereco = contrato.Cobranca.Endereco;
            servico.cobranca_cep = Tools.cep(endereco.cep);
            servico.cobranca_endereco = endereco.logradouro;
            servico.cobranca_endereco_numero = endereco.numero ?? 0;
            servico.cobranca_endereco_complemento = endereco.complemento ?? '';
            servico.cobranca_endereco_referencia = endereco.pontoreferencia ?? '';
            servico.cobranca_endereco_bairro = endereco.bairro;
            servico.cobranca_cidade = endereco.cidade;
            servico.cobranca_uf = endereco.uf;
            servico.cobranca_coordenadas_gps = '';
            servico.cobranca_cidade_ibge = '';

            let ibge = MunicipiosIbge.first('uf_sigla = ? AND municipio_nome = ?', [
                servico.cobranca_uf,
                servico.cobranca_cidade
            ]);

            if (ibge) {
                servico.cobranca_cidade_ibge = `${ibge.uf_codigo}${ibge.municipio_codigo}`;
            }
        }


        servico.dici_scm_tipo_atendimento = 'URBANO';
        servico.dici_scm_tecnologias_id = 13;
        servico.tecnologia = 'h';

        servico.valor_desconto = 0;
        servico.valor_acrescimo = 0;

        // if (contrato.fidelidade_data) {
        //     servico.fidelidade = 1;
        //     servico.fidelidade_valor_total = 0;
        //     servico.fidelidade_data_inicio = contrato.fidelidade_data;
        //     servico.fidelidade_valor_bonificacao = 0;
        //     servico.fidelidade_tempo
        // }
        if (contrato.Cobranca.isento > 0 && contrato.Cobranca.isento < 100) {
            desconto = new DescontosAcrescimos();
            desconto.clientes_has_internetplanos_id = servico.id;
            desconto.data_inicial = '0000-00-00';
            desconto.tipo = 'DESCONTO';
            desconto.valor = contrato.Cobranca.isento;
            desconto.tipo_valor = 'PERCENTUAL';
            desconto.motivo = '';
            desconto.removido = 0;
            desconto.data_hora_cadastro = servico.data_cadastro;
        }
        // if (contrato.acrescimo > 0) {
        //     acrescimo = new DescontosAcrescimos();
        //     acrescimo.clientes_has_internetplanos_id = servico.id;
        //     acrescimo.data_inicial = '0000-00-00';
        //     acrescimo.tipo = 'ACRESCIMO';
        //     acrescimo.valor = contrato.acrescimo;
        //     acrescimo.tipo_valor = 'VALOR';
        //     acrescimo.motivo = '';
        //     acrescimo.removido = 0;
        //     acrescimo.data_hora_cadastro = servico.data_cadastro;
        // }

        servico.radio_login = '';
        servico.radio_senha = '';
        servico.wpa_login = '';
        servico.wpa_senha = '';
        servico.suspensao_motivo = '';
        servico.ip_gateway = 0;
        servico.ip_mascara = 0;
        servico.ip_rede = 0;
        servico.ip_broadcast = 0;
        servico.mikrotik_interfaces_id = 0;
        servico.bloqueio_hora_dias = '';

        try {
            servico.setLogSistema(false);
            servico.Insert();
            if (desconto) {
                desconto.Insert();
            }
            // if (acrescimo) {
            //     acrescimo.Insert();
            // }
        } catch (e) {
            var_dump(servico, e.toString());
            // // var_dump(ModelBase.getConnection("duobox"));
            // conn.setDb(ModelBase.getConnection("duobox"));
            // var_dump(conn.getErrorMsg(), conn.getErrorDb(), conn.getErrorNo());
            exit();
        }
    });
}


export function migra() {
    // console.log('Buscando serviços adicionais para migrar...');
    // let adicionais: SisAdicional[] = SisAdicional.all();
    // let totalPlans = adicionais.length
    // let actualPlan = 0;
    // for (let adicionalIndex in adicionais) {
    //     let adicional = adicionais[adicionalIndex];
    //     let servico = adicional.SisCliente;
    //     servico.clientes_id = servico.id;
    //     servico.id = null;
    //     servico.tipo = adicional.tipo;
    //     servico.mac = adicional.mac;
    //     servico.bloqueado = adicional.bloqueado;
    //     servico.login = adicional.username;
    //     servico.senha = adicional.senha;
    //     servico.ip = adicional.ip;
    //     insereServico(servico);
    //     console_tools.mostraProgresso(++actualPlan, totalPlans, `Migrando servico ${servico.clientes_id} - ${servico.login}`);
    // }
    // console.log('');
}
