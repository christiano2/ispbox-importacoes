import { base as ModelBase, MunicipiosIbge, Clientes, ClientesContatos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import AdmcoreCliente from "../models/AdmcoreCliente";
import { TIPO_PESSOA } from "../models/AdmcorePessoa";
import { numerico } from "powerjs/support/helper";
import * as DadosServicoInternet from './ServicosInternet';
import * as Tools from "../../lib/tools";
import console_tools from "powerjs/support/console_tools";

export function clear() {
    console.log('Limpando dados de clientes...');
    // setamos o conn para usar a conexão padrao do mvc
    conn.setDb(ModelBase.getConnection('duobox'));

    conn.beginTransaction();
    try {
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes');
        conn.exec('TRUNCATE clientes_contatos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "CLIENTE"');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
        console.log('Tabela de clientes truncada');
    } catch(err) {
        conn.rollback();
        console.log(`Erro limpado clientes: ${err.getMessage()}`);
        console.log(err);
        exit();
    }
}

export function migra() {
    // let subs: SisCliente[] = SisCliente.all('', false, { 'limit': 10, 'offset': 0 });
    console.log('Buscando cadastros de clientes');
    let clientes: AdmcoreCliente[] = AdmcoreCliente.all();
    let totalClientes = clientes.length;
    let clienteAtual = 0;

    let ddd = '75';

    clientes.forEach((clienteSgp) => {
        console_tools.mostraProgresso(++clienteAtual, totalClientes, `Migrando cliente ${clienteSgp.id} - ${clienteSgp.Pessoa.nome}`);
        let cliente = new Clientes();
        let contatos: ClientesContatos[] = [];

        // sub.celular = numerico.apenasNumero(sub.celular ?? '');
        // sub.fone = numerico.apenasNumero(sub.fone ?? '');
        // if (sub.celular.length == 16) {
        //     celular3 = Tools.telefone(sub.celular.substring(8), ddd);
        //     sub.celular = sub.celular.substring(0,8);
        // }
        // sub.fone = Tools.telefone(sub.fone, ddd);
        // sub.celular = Tools.telefone(sub.celular, ddd);
        cliente.id = clienteSgp.id;
        cliente.nome = clienteSgp.Pessoa.nome;

        cliente.data_nascimento = clienteSgp.Pessoa.datanasc ?? '0000-00-00';
        cliente.sexo = clienteSgp.Pessoa.sexo ?? 'NI';

        cliente.estado_civil = 0;
        cliente.nome_pai = clienteSgp.Pessoa.nomepai ?? '';
        cliente.nome_mae = clienteSgp.Pessoa.nomemae ?? '';
        cliente.inscricao_municipal = '';
        cliente.responsavel = '';

        if (clienteSgp.Pessoa.tipopessoa == TIPO_PESSOA.FISICA) {
            cliente.tipo_pessoa = Clientes.TIPO_PESSOA_FISICA;
            cliente.cpf = numerico.apenasNumero(clienteSgp.Pessoa.cpfcnpj ?? '');
            cliente.rg = (clienteSgp.Pessoa.rg ?? '') + (clienteSgp.Pessoa.rg_emissor ?? '');
        }
        else {
            cliente.tipo_pessoa = Clientes.TIPO_PESSOA_JURIDICA;
            cliente.cnpj = numerico.apenasNumero(clienteSgp.Pessoa.cpfcnpj ?? '');
            cliente.nome_fantasia = clienteSgp.Pessoa.nomefantasia ?? '';
            cliente.inscricao_estadual = clienteSgp.Pessoa.insc_estadual ?? '';
            cliente.inscricao_municipal = clienteSgp.Pessoa.insc_municipal ?? '';
            cliente.responsavel = (clienteSgp.Pessoa.resempresa ?? '') + (clienteSgp.Pessoa.rescpf ?? '');
        }

        cliente.exterior = 0;
        cliente.documento_exterior = 0;
        cliente.ruc = '';
        cliente.pais = '';
        cliente.profissao = '';
        cliente.local_trabalho = '';
        cliente.produtor_rural = 0;
        cliente.email = clienteSgp.getEmail(0);
        cliente.email2 = clienteSgp.getEmail(1);
        cliente.telefone = Tools.telefone(clienteSgp.getTelefone(0), ddd);
        cliente.celular = Tools.telefone(clienteSgp.getTelefone(1), ddd);

        if (clienteSgp.getEmail() && clienteSgp.getEmail().length > 2) {
            for (let i = 2; i <= clienteSgp.getEmail()-1; i++) {
                let contato = new ClientesContatos();
                contato.clientes_id = cliente.id;
                contato.tipo = 'FINANCEIRO';
                contato.nome = cliente.nome;
                contato.email = clienteSgp.getEmail(i);
                contato.observacoes = '';
                contatos.push(contato);
            }
        }
        if (clienteSgp.getTelefone() && clienteSgp.getTelefone().length > 2) {
            for (let i = 2; i <= clienteSgp.getTelefone()-1; i++) {
                let contato = new ClientesContatos();
                contato.clientes_id = cliente.id;
                contato.tipo = 'FINANCEIRO';
                contato.nome = cliente.nome;
                contato.telefone = Tools.telefone(clienteSgp.getTelefone(i), ddd);
                contato.observacoes = '';
                contatos.push(contato);
            }
        }

        cliente.cep = Tools.cep(clienteSgp.Endereco.cep);
        cliente.endereco = clienteSgp.Endereco.logradouro;
        cliente.endereco_numero = clienteSgp.Endereco.numero ?? 0;
        cliente.endereco_complemento = clienteSgp.Endereco.complemento ?? '';
        cliente.endereco_referencia = clienteSgp.Endereco.pontoreferencia ?? '';
        cliente.endereco_bairro = clienteSgp.Endereco.bairro;
        cliente.cidade = clienteSgp.Endereco.cidade;
        cliente.uf = clienteSgp.Endereco.uf;
        cliente.coordenadas_gps = '';
        cliente.cidade_ibge = '';

        let ibge = MunicipiosIbge.first('uf_sigla = ? AND municipio_nome = ?', [
            cliente.uf,
            cliente.cidade
        ]);

        if (ibge) {
            cliente.cidade_ibge = `${ibge.uf_codigo}${ibge.municipio_codigo}`;
        }

        cliente.cobranca_igual = 1;
        cliente.cobranca_endereco_referencia = '';
        cliente.cobranca_cidade_ibge = '';
        cliente.cobranca_pais = '';
        cliente.cobranca_coordenadas_gps = '';

        cliente.cadastro_usuarios_id = 0;
        cliente.alteracao_usuarios_id = 0;

        cliente.documento_bloqueado_observacao = '';

        let dataCadastro = clienteSgp.getDataCadastro;

        cliente.data_cadastro = dataCadastro.format('Y-m-d');

        let dataAlteracao = clienteSgp.getDataAlteracao;
        cliente.data_alteracao = dataAlteracao.format('Y-m-d');

        try {
            cliente.setLogSistema(false);
            cliente.Insert();
            if (contatos.length > 0) {
                contatos.forEach((contato) => {
                    contato.setLogSistema(false);
                    contato.Save();
                });
            }
            let totalContratos = clienteSgp.Clientecontrato.length;
            if (totalContratos > 0) {
                DadosServicoInternet.insereServicos(clienteSgp);
            }
        } catch (e) {
            var_dump(e.message ?? e.getMessage());
            exit();
        }
        // chamada para inserir o serviço
    });

    console.log();
}
