import conn from "powerjs/db/conn";
import models from "powerjs/db/models";

const baseNamespace = 'powerjs\\Models\\SGP';

function dbConnect(database) {
    // conectamos no banco de dados
    const dbConn = conn.newADOConnection(`postgres9://${database.login}:${database.password}@${database.host}/${database.dbname}?new=true`);

    if (dbConn === false) {
        console.log('erro ao tentar conectar no banco de dados');
        exit();
        return;
    }

    // setamos o schema erp cpara busca das tabelas
    if ('schema' in database) {
        // @ts-ignore
        dbConn.Execute(`SET search_path TO ${database.schema}`);
    }

    // setamos que os models criados pelo powerjs devem usar esta conexão
    models.addConnection(baseNamespace, dbConn);
    return dbConn;
}

export { dbConnect, baseNamespace }
