import { SGPModel } from "./common";
import AdmcoreCliente from "./AdmcoreCliente";
import FinanceiroCobranca from "./FinanceiroCobranca";

export default class FinanceiroTitulo extends SGPModel {
    Cliente: AdmcoreCliente;
    Cobranca: FinanceiroCobranca;

    static STATUS_ABERTO: 1;
    static STATUS_PAGO: 2
    static STATUS_CANCELADO: 3;

    static relations: Record<string, any> = {
		belongsTo: {
			Cliente: AdmcoreCliente.className(),
			Cobranca: FinanceiroCobranca.className()
		}
    }

    static {
        this.initQueue();
    }
}
