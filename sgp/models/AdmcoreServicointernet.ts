import { SGPModel } from "./common";
import AdmcoreEndereco from "./AdmcoreEndereco";
import AdmcorePlanointernet from "./AdmcorePlanointernet";

export default class AdmcoreServicointernet extends SGPModel {
    Endereco?: AdmcoreEndereco;
    Planointernet?: AdmcorePlanointernet;
    static relations: Record<string, any> = {
        belongsTo: {
            Endereco: AdmcoreEndereco.className(),
            Planointernet: AdmcorePlanointernet.className()
        }
    }

    static {
        this.initQueue();
    }
}
