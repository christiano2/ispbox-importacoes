import { SGPModel } from "./common";
import AdmcoreServicointernet from "./AdmcoreServicointernet";
import AdmcoreClientecontratocentralacesso from "./AdmcoreClientecontratocentralacesso";
import FinanceiroCobranca from "./FinanceiroCobranca";

export enum CONTRATO_STATUS {
    STATUS_LIBERADO= 1,
    STATUS_CANCELADO= 3,
    STATUS_BLOQUEADO= 4,
    STATUS_SUSPENSAO_PARCIAL = 7
}

class AdmcoreClientecontratostatus extends SGPModel {
    static STATUS_CANCELADO = 3;


    static relations: Record<string, any> = {
    }

    static {
        this.initQueue();
    }
}

export default AdmcoreClientecontratostatus;
export { AdmcoreClientecontratostatus };