import { SGPModel } from "./common";
import AdmcorePessoa from "./AdmcorePessoa";
import AdmcoreEndereco from "./AdmcoreEndereco";

export default class AdmcoreClientecontratocentralacesso extends SGPModel {
    Pessoa: AdmcorePessoa[];
    Endereco: AdmcoreEndereco[];

    static relations: Record<string, any> = {
		hasMany: {
			Pessoa: AdmcorePessoa.className(),
			Endereco: AdmcoreEndereco.className()
		}
    }

    static {
        this.initQueue();
    }
}
