import { SGPModel } from "./common";
import AdmcorePessoa from "./AdmcorePessoa";
import AdmcoreEndereco from "./AdmcoreEndereco";
import AdmcoreClientecontato from "./AdmcoreClientecontato";
import AdmcoreClientecontrato from "./AdmcoreClientecontrato";
import numerico from "powerjs/support/helper/numerico";

const TIPOS_CONTATOS = {
    'TELEFONE': [
        'TELEFONE_FIXO_RESIDENCIAL',
        'CELULAR_PESSOAL'
    ],
    'EMAIL': [
        'EMAIL'
    ]
}

export default class AdmcoreCliente extends SGPModel {
    Pessoa: AdmcorePessoa;
    Endereco: AdmcoreEndereco;

    Clientecontato: AdmcoreClientecontato[];
    Clientecontrato: AdmcoreClientecontrato[];

    static relations: Record<string, any> = {
		belongsTo: {
			Pessoa: AdmcorePessoa.className(),
			Endereco: AdmcoreEndereco.className()
		},
        hasMany: {
            Clientecontrato: {
                class: AdmcoreClientecontrato.className(),
                foreignKey: 'cliente_id'
            },
            Clientecontato: {
                class: AdmcoreClientecontato.className(),
                foreignKey: 'cliente_id'
            }
        }
    }

    static {
        this.initQueue();
    }

    getTelefone(posicao?: number) {
        return this.getContatosTipo('TELEFONE', posicao);
    }

    getEmail(posicao?: number) {
        return this.getContatosTipo('EMAIL', posicao);
    }

    getContatosTipo(tipo: string, posicao?: number) {
        tipo = TIPOS_CONTATOS[tipo] ? tipo : 'TELEFONE';

        let contatos = [];
        for (let contatoIdx in this.Clientecontato) {
            let contato = this.Clientecontato[contatoIdx].Contato;
            if (TIPOS_CONTATOS[tipo].indexOf(contato.tipo) > -1) {
                if (tipo == 'TELEFONE') {
                    contatos.push(numerico.apenasNumero(contato.contato));
                }
                else {
                    contatos.push(contato.contato);
                }
            }
        }

        if (posicao === null) {
            return contatos;
        }

        return contatos[posicao] ?? null;
    }
}
