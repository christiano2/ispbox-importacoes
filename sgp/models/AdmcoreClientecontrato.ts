import { SGPModel } from "./common";
import AdmcoreServicointernet from "./AdmcoreServicointernet";
import AdmcoreClientecontratocentralacesso from "./AdmcoreClientecontratocentralacesso";
import FinanceiroCobranca from "./FinanceiroCobranca";
import AdmcoreClientecontratostatus from "./AdmcoreClientecontratostatus";

export default class AdmcoreClientecontrato extends SGPModel {
    ServicoInternet: AdmcoreServicointernet;
    CentralAcesso: AdmcoreClientecontratocentralacesso;
    Cobranca: FinanceiroCobranca;
    Status: AdmcoreClientecontratostatus;

    static relations: Record<string, any> = {
        belongsTo: {
            Cobranca: FinanceiroCobranca.className(),
            Status: AdmcoreClientecontratostatus.className()
        },
        hasOne: {
            ServicoInternet: {
                class: AdmcoreServicointernet.className(),
                foreignKey: 'clientecontrato_id'
            },
            CentralAcesso: {
                class: AdmcoreClientecontratocentralacesso.className(),
                foreignKey: 'cliente_contrato_id'
            }
        }
    }

    static {
        this.initQueue();
    }
}
