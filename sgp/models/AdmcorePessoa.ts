import { SGPModel } from "./common";

enum TIPO_PESSOA {
    FISICA = 'F',
    JURIDICA = 'J'
}

export { TIPO_PESSOA };

export default class AdmcorePessoa extends SGPModel {
    tipopessoa: TIPO_PESSOA;

    static {
        this.initQueue();
    }
}
