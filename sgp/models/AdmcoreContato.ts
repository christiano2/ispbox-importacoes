import { SGPModel } from "./common";

export default class AdmcoreContato extends SGPModel {
    contato: string;
    tipo: string;
    static {
        this.initQueue();
    }
}
