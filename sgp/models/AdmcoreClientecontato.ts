import { SGPModel } from "./common";
import AdmcoreContato from "./AdmcoreContato";

export default class AdmcoreClientecontato extends SGPModel {
    Contato: AdmcoreContato;

    static relations: Record<string, any> = {
		belongsTo: {
			Contato: AdmcoreContato.className()
		}
    }

    static {
        this.initQueue();
    }
}
