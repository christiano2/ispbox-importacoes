import { SGPModel } from "./common";
import AdmcorePlano from "./AdmcorePlano";

export default class AdmcorePlanointernet extends SGPModel {
    Plano: AdmcorePlano;

    static relations: Record<string, any> = {
        belongsTo: {
            Plano: AdmcorePlano.className()
        }
    }

    static {
        this.initQueue();
    }
}
