import { SGPModel } from "./common";
import AdmcorePessoa from "./AdmcorePessoa";
import AdmcoreEndereco from "./AdmcoreEndereco";
import FinanceiroVencimento from "./FinanceiroVencimento";
import FiscalCfop from "./FiscalCfop";
// import AdmcoreClientecontrato from "./AdmcoreClientecontrato";

export default class FinanceiroCobranca extends SGPModel {
    Pessoa: AdmcorePessoa;
    Endereco: AdmcoreEndereco;
    Vencimento: FinanceiroVencimento;
    NotafiscalCfop: FiscalCfop;
    // Clientecontrato: AdmcoreClientecontrato;


    static relations: Record<string, any> = {
		belongsTo: {
			Pessoa: AdmcorePessoa.className(),
			Endereco: AdmcoreEndereco.className(),
            Vencimento: FinanceiroVencimento.className(),
            NotafiscalCfop: FiscalCfop.className()
		},
        hasOne: {
            Clientecontrato: {
                class: SGPModel.namespaceClass('AdmcoreClientecontrato'),
                foreignKey: 'cobranca_id'
            }
        }
    }

    static {
        this.initQueue();
    }
}
