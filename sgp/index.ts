import { dbConnect } from "./common";
import { sgp as config } from "../config.json";
import * as DadosPlanos from "./dados/Planos";
import * as DadosClientes from "./dados/Clientes";
import * as DadosServicosInternet from "./dados/ServicosInternet";
import * as DadosCobrancas from "./dados/Cobrancas";
import { Model } from "../lib/model";

// primeiro iniciamos a conexão
const dbConnection = dbConnect(config.database);

// setupModels();
Model.initModels();

export { dbConnection };

export function run() {

    // DadosClientes.clear();
    // DadosPlanos.clear();
    // DadosServicosInternet.clear();
    // //
    // //
    // DadosPlanos.migra();
    // DadosClientes.migra();
    // console.log('running');
    // // migra os adicionais pois os serviços são migrados junto com os dados dos clientes
    // // assim evitamos selecionar 2x os registros dos clientes
    // DadosServicosInternet.migra();

    DadosCobrancas.clear();
    DadosCobrancas.migra();

}

