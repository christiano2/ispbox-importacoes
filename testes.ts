// import * as SGP from "./sgp/sgp.ts";
import * as Voalle from "./voalle/voalle.ts";

/**
 * Teste Voalle
 */

console.log('**** Voalle ****');

console.log('busca pessoa...');
let people = Voalle.models.People.firstId(20603);
console.log(`Nome: ${people.name}`);
console.log('Buscando endereços da pessoa...');
console.log(`Total de endereços: ${people.PeopleAddresses.length}`);
if (people.PeopleAddresses.length > 0) {
    console.log(`Primeiro: ${people.PeopleAddresses[0].street}`);
}

// teste de chamada estática criada no model do TS
// console.log(Voalle.models.People.primeiroPF());


console.log('');
/**
 * Teste SGP
 */

//  console.log('**** SGP ****');

// let cliente = SGP.models.AdmcoreCliente.first();

// console.log(`Código do primeiro cliente: ${cliente.id} - Data de cadastro: ${cliente.data_cadastro}`);
// console.log(`Nome da pessoa: ${cliente.Pessoa.nome}`);

