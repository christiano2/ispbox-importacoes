import { dbConnect } from "./common";
import { quaza as config } from "../config.json";
import { Model } from "../lib/model";
import * as DadosClientes from './dados/Clientes';
import * as DadosPlanos from './dados/Planos';
import * as DadosServicosInternet from './dados/ServicosInternet';
import * as DadosCobrancas from './dados/Cobrancas';

// primeiro iniciamos a conexão
export const dbConnection = dbConnect(config.database);

Model.initModels();

export function run() {
    // DadosCobrancas.clear();
    DadosClientes.clear();
    DadosServicosInternet.clear();
    // DadosPlanos.clear();
    // // //
    // DadosPlanos.migra();
    // // // // Os serviços já são mgirados aqui
    DadosClientes.migra();

    // DadosCobrancas.migra();

}

