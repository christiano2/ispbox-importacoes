import { base as ModelBase, Clientes, ClientesContatos, MunicipiosIbge, AnotacoesCadastros } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import { Clientes as ClientesQ } from "../models/Clientes";
import {data, numerico} from "powerjs/support/helper";
import * as DadosServicoInternet from './ServicosInternet';
import * as Tools from "../../lib/tools";
import console_tools from "powerjs/support/console_tools";

export function clear() {
    console.log('Limpando dados de clientes...');
    // setamos o conn para usar a conexão padrao do mvc
    conn.setDb(ModelBase.getConnection('duobox'));

    conn.beginTransaction();
    try {
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes');
        conn.exec('TRUNCATE clientes_contatos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "CLIENTE"');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
        console.log('Tabela de clientes truncada');
    } catch(err) {
        conn.rollback();
        console.log(`Erro limpado clientes: ${err.getMessage()}`);
        console.log(err);
        exit();
    }
}

export function migra() {
    // let subs: SisCliente[] = SisCliente.all('', false, { 'limit': 10, 'offset': 0 });
    console.log('Buscando cadastros de clientes');
    let clientes: ClientesQ[] = ClientesQ.all('id > 0');
    let totalClientes = clientes.length;
    let clienteAtual = 0;

    let ddd = '21';
    let municipiosCache: MunicipiosIbge[] = [];

    clientes.forEach((clienteQ) => {
        console_tools.mostraProgresso(++clienteAtual, totalClientes, `Migrando cliente ${clienteQ.id} - ${clienteQ.nome}`);
        if (clienteQ.tipo_cliente != '' && clienteQ.nome != '') {
            let cliente = new Clientes();
            let contatos: ClientesContatos[] = [];
            let anotacoes: AnotacoesCadastros[] = [];

            // sub.celular = numerico.apenasNumero(sub.celular ?? '');
            // sub.fone = numerico.apenasNumero(sub.fone ?? '');
            // if (sub.celular.length == 16) {
            //     celular3 = Tools.telefone(sub.celular.substring(8), ddd);
            //     sub.celular = sub.celular.substring(0,8);
            // }
            // sub.fone = Tools.telefone(sub.fone, ddd);
            // sub.celular = Tools.telefone(sub.celular, ddd);
            cliente.id = clienteQ.id;
            cliente.nome = clienteQ.nome;

            cliente.data_nascimento = clienteQ.datanasc ?? '0000-00-00';
            cliente.sexo = clienteQ.sexo;

            cliente.estado_civil = 0;
            cliente.nome_pai = clienteQ.pai ?? '';
            cliente.nome_mae = clienteQ.mae ?? '';
            cliente.inscricao_municipal = '';
            cliente.responsavel = '';
            cliente.profissao = '';
            cliente.local_trabalho = '';

            if (clienteQ.tipoPessoa == Clientes.TIPO_PESSOA_FISICA) {
                cliente.tipo_pessoa = Clientes.TIPO_PESSOA_FISICA;
                cliente.cpf = numerico.apenasNumero(clienteQ.cpfcnpj ?? '');
                cliente.rg = clienteQ.rg ?? '';
                cliente.profissao = '';
                cliente.local_trabalho = '';
            } else {
                cliente.tipo_pessoa = Clientes.TIPO_PESSOA_JURIDICA;
                cliente.cnpj = numerico.apenasNumero(clienteQ.cpfcnpj ?? '');
                cliente.nome_fantasia = clienteQ.fantasia ?? '';
                cliente.inscricao_estadual = clienteQ.ie ?? '';
                cliente.inscricao_municipal = clienteQ.im ?? '';
                cliente.responsavel = clienteQ.responsavel ?? '';
            }

            cliente.exterior = 0;
            cliente.documento_exterior = 0;
            cliente.ruc = '';
            cliente.pais = '';
            cliente.produtor_rural = 0;
            cliente.email = clienteQ.email;
            cliente.email2 = clienteQ.emailalt;

            function montaListaTelefones(clienteQ: ClientesQ) {
                let phoneFields = [
                    'telefoneres',
                    'telefonecom',
                    'telefonecel',
                    'telSms'
                ]
                let telefones = [];
                phoneFields.forEach((campoTelefone) => {
                    let tmpTelefone = Tools.telefone(clienteQ[campoTelefone], ddd);
                    if (tmpTelefone && telefones.indexOf(tmpTelefone) == -1 && tmpTelefone != '0000000000' && tmpTelefone != '00000000000') {
                        telefones.push(tmpTelefone);
                    }
                })
                return telefones;
            }

            let telefones = montaListaTelefones(clienteQ);

            cliente.telefone = telefones[0] ?? '';
            cliente.celular = telefones[1] ?? '';

            if (telefones.length > 2) {
                for (let i = 2; i <= telefones.length-1; i++) {
                    let contato = new ClientesContatos();
                    contato.clientes_id = cliente.id;
                    contato.tipo = 'FINANCEIRO';
                    contato.nome = cliente.nome;
                    contato.telefone = telefones[i];
                    contato.observacoes = '';
                    contatos.push(contato);
                }
            }

            cliente.cep = Tools.cep(clienteQ.cep);
            cliente.endereco = clienteQ.endereco;

            cliente.endereco_numero = numerico.apenasNumero(clienteQ.numero) ?? 0;
            cliente.endereco_complemento = clienteQ.complemento ?? '';
            cliente.endereco_referencia = clienteQ.referencia ?? '';
            cliente.endereco_bairro = clienteQ.bairro;
            cliente.cidade = '';
            cliente.uf = '';
            cliente.coordenadas_gps = clienteQ.geo_lat + ' ' + clienteQ.geo_lng;
            cliente.cidade_ibge = clienteQ.cidade;

            if (municipiosCache[cliente.cidade_ibge]) {
                cliente.cidade = municipiosCache[cliente.cidade_ibge].municipio_nome;
                cliente.uf = municipiosCache[cliente.cidade_ibge].uf_sigla;
            }
            else {
                let cidadeInfo = MunicipiosIbge.getByCodigoIbge(cliente.cidade_ibge);

                if (cidadeInfo) {
                    municipiosCache[cliente.cidade_ibge] = cidadeInfo;
                    cliente.cidade = cidadeInfo.municipio_nome;
                    cliente.uf = cidadeInfo.uf_sigla;
                }
            }

            if (clienteQ.geo_info && clienteQ.geo_info.trim() != '') {
                let anotacao = new AnotacoesCadastros();
                anotacao.origem_tipo = 'CLIENTE';
                anotacao.origem_id = cliente.id;
                anotacao.data_hora = clienteQ.criacao ?? data.convertAnyToDateTime('').format('Y-m-d H:i:s');
                anotacao.anotacao = 'geoInfo: ' + clienteQ.geo_info;
                anotacao.usuarios_id = 1;
                anotacoes.push(anotacao);
            }

            if (clienteQ.obs && clienteQ.obs.trim() != '') {
                let anotacao = new AnotacoesCadastros();
                anotacao.origem_tipo = 'CLIENTE';
                anotacao.origem_id = cliente.id;
                anotacao.data_hora = clienteQ.criacao ?? data.convertAnyToDateTime('').format('Y-m-d H:i:s');
                anotacao.anotacao = 'obs: ' + clienteQ.obs;
                anotacao.usuarios_id = 1;
                anotacoes.push(anotacao);
            }

            if (clienteQ.observacao && clienteQ.observacao.trim() != '') {
                let anotacao = new AnotacoesCadastros();
                anotacao.origem_tipo = 'CLIENTE';
                anotacao.origem_id = cliente.id;
                anotacao.data_hora = clienteQ.criacao ?? data.convertAnyToDateTime('').format('Y-m-d H:i:s');
                anotacao.anotacao = 'observacao: ' + clienteQ.observacao;
                anotacao.usuarios_id = 1;
                anotacoes.push(anotacao);
            }

            if (clienteQ.mensagem && clienteQ.mensagem.trim() != '') {
                let anotacao = new AnotacoesCadastros();
                anotacao.origem_tipo = 'CLIENTE';
                anotacao.origem_id = cliente.id;
                anotacao.data_hora = clienteQ.criacao ?? data.convertAnyToDateTime('').format('Y-m-d H:i:s');
                anotacao.anotacao = 'mensagem: ' + clienteQ.mensagem;
                anotacao.usuarios_id = 1;
                anotacoes.push(anotacao);
            }

            cliente.cobranca_igual = 1;
            cliente.cobranca_endereco_referencia = '';
            cliente.cobranca_cidade_ibge = '';
            cliente.cobranca_pais = '';
            cliente.cobranca_coordenadas_gps = '';

            cliente.cadastro_usuarios_id = 0;
            cliente.alteracao_usuarios_id = 0;

            cliente.documento_bloqueado_observacao = '';

            cliente.data_cadastro = clienteQ.criacao;

            cliente.data_alteracao = clienteQ.alteracao;

            try {
                cliente.setLogSistema(false);
                cliente.Insert();
                if (contatos.length > 0) {
                    contatos.forEach((contato) => {
                        contato.setLogSistema(false);
                        contato.Save();
                    });
                }
                if (anotacoes.length > 0) {
                    anotacoes.forEach((anotacao) => {
                        anotacao.setLogSistema(false);
                        anotacao.Save();
                    });
                }
                let totalContratos = clienteQ.Contrato.length;
                if (totalContratos > 0) {
                    DadosServicoInternet.insereServicos(clienteQ);
                }
            } catch (e) {
                console.log(e.params);
                var_dump(e.message ?? e.getMessage());
                let type;
                let params = '';
                e.params.forEach((value, index) => {
                    console.log(index,value);
                    if (index == 0) {
                        type = value.split('');
                    }
                    else {
                        console.log('type', type[index-1]);
                        if (value == null) {
                            params += 'null,';
                        }
                        else if (type[index-1] == 's') {
                            params += '"'+value+'",';
                        }
                        else {
                            params += value+',';
                        }
                    }
                })
                console.log(params);
                exit();
            }
        }
        // chamada para inserir o serviço
    });

    console.log();
}
