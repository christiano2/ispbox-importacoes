import { base as ModelBase, ClientesHasInternetplanos, MunicipiosIbge } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import Clientes from "../models/Clientes";
import * as Tools from "../../lib/tools";
import AnotacoesCadastros from "powerjs/db/models/AnotacoesCadastros";
import {data, numerico} from "powerjs/support/helper";
import ContratoLogin from "../models/ContratoLogin";
// import ContratoLogin from "../models/ContratoLogin";

export function clear() {
    console.log('Limpando serviços de internet...');
    conn.setDb(ModelBase.getConnection("duobox"));
    try {
        conn.beginTransaction();
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE clientes_has_internetplanos');
        conn.exec('DELETE FROM anotacoes_cadastros WHERE origem_tipo = "INTERNET"');
        conn.exec('TRUNCATE descontos_acrescimos');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado serviços de internet: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabela de serviços de internet truncada');
}

const servicoAtivo = { // estadoContrato
    '1' : 1, // ativo
    '2' : 1, // bloqueado automatico
    '3' : 1, // bloqueado manual
    '4' : 0, // cancelado
    '5' : 1, // desativado
    '6' : 0, // pré cadastro,
    default : 1
};

export function insereServicos(clienteQ: Clientes) {

    clienteQ.Contrato.forEach((contrato) => {
        if (!contrato.ContratoLogin) {
            contrato.ContratoLogin = new ContratoLogin();
            contrato.ContratoLogin.idGroup = 1;
            contrato.ContratoLogin.user = 'contrato_'+contrato.idCliente+'_'+contrato.id;
            contrato.ContratoLogin.pass = '123456';
        }
        // if (contrato.ContratoLogin) {
            let desconto = null;
            let acrescimo = null;
            let observacao = null;
            let servico = new ClientesHasInternetplanos();

            servico.id = contrato.id;
            servico.clientes_id = contrato.idCliente;
            servico.internetplanos_id = contrato.ContratoLogin.idGroup;
            servico.empresas_id = 1;
            // let bancoId = parseInt(contrato.Cobranca.portador_id);
            // servico.bancos_id = (bancoId !== bancoId) ? 1 : bancoId + 1;
            servico.bancos_id = parseInt(contrato.idCarteira) + 1;
            // servico.tipo_conexao = tipoConexao[contrato.ServicoInternet.tipoconexao] || tipoConexao.default;
            servico.tipo_conexao = 'PPPOE';
            servico.radio_mac = (contrato.mac && contrato.mac != '00:00:00:00:00:00') ? contrato.mac.replace(':', '') : '';

            servico.status = 1;

            servico.bloqueio_automatico = contrato.bloqueioAutomatico == 1 ? 1 : 0;
            servico.pppoe_login = contrato.ContratoLogin.user;
            servico.pppoe_senha = contrato.ContratoLogin.pass;
            servico.pppoe_tipo_ip = 'DINAMICO';
            servico.pppoe_ip = 0;
            let ipLong = contrato.ip ? Tools.ip2long(contrato.ip) : 0;
            if ((servico.tipo == 'PPPOE') && (ipLong > 0)) {
                servico.pppoe_tipo_ip = 'FIXO';
                servico.pppoe_ip = ipLong;
            }
            servico.hotspot_filtro_ip = 0;
            servico.filtro_mac = servico.radio_mac;
            servico.dia_vencimento = contrato.vencimento;
            servico.nota_fiscal_cfop = null;
            if (!servico.nota_fiscal_cfop) {
                servico.nota_fiscal_cfop = (clienteQ.tipoPessoa == 'F') ? 5307 : 5303;
            }
            servico.nota_fiscal_tipo_assinante = (clienteQ.tipoPessoa == 'F') ? 3 : 1;
            servico.nota_fiscal_21_informacoes_complementares = '';
            servico.valor_instalacao = 0;
            servico.quantidade_parcelas_instalacao = 0;
            servico.data_instalacao = contrato.dataInstalacao;
            servico.prazo_contrato = 12;
            servico.meses_gratuitos = 0;
            servico.ponto_acesso = '';

            servico.equipamento_instalado = 'COMODATO';
            servico.data_cadastro = contrato.criacao;

            servico.data_alteracao = contrato.alteracao;

            servico.motivo_rescisao = '';
            servico.data_rescisao = '0000-00-00';
            if (servico.ativo == 0) {
                servico.motivo_rescisao = contrato.motivoCancelamento;
                servico.data_rescisao = (contrato.dataCancelamento && contrato.dataCancelamento != '0000-00-00') ? contrato.dataCancelamento : contrato.dataEstado;
            }
            servico.rescisao_usuarios_id = 0;
            servico.cadastro_usuarios_id = 0;
            servico.alteracao_usuarios_id = 0;
            servico.debito_agencia = '';
            servico.debito_id_cliente_empresa = '';
            servico.debito_id_cliente_banco = '';
            servico.ativo = servicoAtivo[contrato.idEstado] ?? servicoAtivo.default;
            if (contrato.idEstado == 5) { // serviço desativado
                servico.status = 2; // contrato suspenso
            }

            servico.isento = (contrato.ContratoServicoContrato && contrato.ContratoServicoContrato.valorServico) == 0 ? 1 : 0;
            servico.desconto_plano_pagamento_antecipado = 1;

            servico.endereco_instalacao_igual_principal = 1;
            if (contrato.ContratoLogin.ContratoLoginEndereco && contrato.ContratoLogin.ContratoLoginEndereco.utilizarContratoLoginEndereco == 1) {
                servico.endereco_instalacao_igual_principal = 0;
                let endereco = contrato.ContratoLogin.ContratoLoginEndereco;
                servico.instalacao_cep = Tools.cep(endereco.cepContratoLoginEndereco);
                servico.instalacao_endereco = endereco.enderecoContratoLoginEndereco;
                servico.instalacao_endereco_numero = numerico.apenasNumero(endereco.numeroContratoLoginEndereco) ?? 0;
                servico.instalacao_endereco_complemento = endereco.complementoContratoLoginEndereco ?? '';
                servico.instalacao_endereco_referencia = endereco.referenciaContratoLoginEndereco ?? '';
                servico.instalacao_endereco_bairro = endereco.bairroContratoLoginEndereco;
                servico.instalacao_cidade = '';
                servico.instalacao_uf = '';
                servico.instalacao_coordenadas_gps = endereco.latitudeContratoLoginEndereco + ' ' + endereco.longitudeContratoLoginEndereco;
                servico.instalacao_cidade_ibge = endereco.idCidadeContratoLoginEndereco;

                let cidadeInfo = MunicipiosIbge.getByCodigoIbge(servico.instalacao_cidade_ibge);

                if (cidadeInfo) {
                    servico.instalacao_cidade = cidadeInfo.municipio_nome;
                    servico.instalacao_uf = cidadeInfo.uf_sigla;
                }

            } else if (contrato.ContratoEndereco.utilizarContratoEndereco == 1) {
                servico.endereco_instalacao_igual_principal = 0;
                let endereco = contrato.ContratoEndereco;
                servico.instalacao_cep = Tools.cep(endereco.cepContratoEndereco);
                servico.instalacao_endereco = endereco.enderecoContratoEndereco;
                servico.instalacao_endereco_numero = numerico.apenasNumero(endereco.numeroContratoEndereco) ?? 0;
                servico.instalacao_endereco_complemento = endereco.complementoContratoEndereco ?? '';
                servico.instalacao_endereco_referencia = endereco.referenciaContratoEndereco ?? '';
                servico.instalacao_endereco_bairro = endereco.bairroContratoEndereco;
                servico.instalacao_cidade = '';
                servico.instalacao_uf = '';
                servico.instalacao_coordenadas_gps = endereco.latitudeContratoEndereco + ' ' + endereco.longitudeContratoEndereco;
                servico.instalacao_cidade_ibge = endereco.idCidadeContratoEndereco;

                let cidadeInfo = MunicipiosIbge.getByCodigoIbge(servico.instalacao_cidade_ibge);

                if (cidadeInfo) {
                    servico.instalacao_cidade = cidadeInfo.municipio_nome;
                    servico.instalacao_uf = cidadeInfo.uf_sigla;
                }
            }

            servico.endereco_cobranca_igual_principal = 1;

            servico.dici_scm_tipo_atendimento = 'URBANO';
            servico.dici_scm_tecnologias_id = 13;
            servico.tecnologia = 'h';

            servico.valor_desconto = 0;
            servico.valor_acrescimo = 0;

            // if (contrato.fidelidade_data) {
            //     servico.fidelidade = 1;
            //     servico.fidelidade_valor_total = 0;
            //     servico.fidelidade_data_inicio = contrato.fidelidade_data;
            //     servico.fidelidade_valor_bonificacao = 0;
            //     servico.fidelidade_tempo
            // }
            // if (contrato.desconto > 0) {
            //     desconto = new DescontosAcrescimos();
            //     desconto.clientes_has_internetplanos_id = servico.id;
            //     desconto.data_inicial = '0000-00-00';
            //     desconto.tipo = 'DESCONTO';
            //     desconto.valor = contrato.desconto;
            //     desconto.tipo_valor = 'VALOR';
            //     desconto.motivo = '';
            //     desconto.removido = 0;
            //     desconto.data_hora_cadastro = servico.data_cadastro;
            // }
            // if (contrato.acrescimo > 0) {
            //     acrescimo = new DescontosAcrescimos();
            //     acrescimo.clientes_has_internetplanos_id = servico.id;
            //     acrescimo.data_inicial = '0000-00-00';
            //     acrescimo.tipo = 'ACRESCIMO';
            //     acrescimo.valor = contrato.acrescimo;
            //     acrescimo.tipo_valor = 'VALOR';
            //     acrescimo.motivo = '';
            //     acrescimo.removido = 0;
            //     acrescimo.data_hora_cadastro = servico.data_cadastro;
            // }

            servico.radio_login = '';
            servico.radio_senha = '';
            servico.wpa_login = '';
            servico.wpa_senha = '';
            servico.suspensao_motivo = '';
            servico.ip_gateway = 0;
            servico.ip_mascara = 0;
            servico.ip_rede = 0;
            servico.ip_broadcast = 0;
            servico.mikrotik_interfaces_id = 0;
            servico.bloqueio_hora_dias = '';

            if (contrato.obs) {
                observacao = new AnotacoesCadastros();
                observacao.origem_tipo = 'INTERNET';
                observacao.origem_id = servico.id;
                observacao.data_hora = servico.data_instalacao ?? data.convertAnyToDateTime('').format('Y-m-d H:i:s');
                observacao.anotacao = contrato.obs;
                observacao.usuarios_id = 1;
            }

            try {
                servico.setLogSistema(false);
                servico.Insert();
                if (desconto) {
                    desconto.setLogSistema(false);
                    desconto.Insert();
                }
                if (acrescimo) {
                    acrescimo.setLogSistema(false);
                    acrescimo.Insert();
                }
                if (observacao) {
                    observacao.setLogSistema(false);
                    observacao.Insert();
                }
            } catch (e) {
                var_dump(servico, e.toString());
                let type;
                let params = '';
                e.params.forEach((value, index) => {
                    console.log(index, value);
                    if (index == 0) {
                        type = value.split('');
                    } else {
                        console.log('type', type[index - 1]);
                        if (value == null) {
                            params += 'null,';
                        } else if (type[index - 1] == 's') {
                            params += '"' + value + '",';
                        } else {
                            params += value + ',';
                        }
                    }
                })
                console.log(params);
                exit();
            }
        // }
    });
}

export function migra() {
    // console.log('Buscando serviços adicionais para migrar...');
    // let adicionais: SisAdicional[] = SisAdicional.all();
    // let totalPlans = adicionais.length
    // let actualPlan = 0;
    // for (let adicionalIndex in adicionais) {
    //     let adicional = adicionais[adicionalIndex];
    //     let servico = adicional.SisCliente;
    //     servico.clientes_id = servico.id;
    //     servico.id = null;
    //     servico.tipo = adicional.tipo;
    //     servico.mac = adicional.mac;
    //     servico.bloqueado = adicional.bloqueado;
    //     servico.login = adicional.username;
    //     servico.senha = adicional.senha;
    //     servico.ip = adicional.ip;
    //     insereServico(servico);
    //     console_tools.mostraProgresso(++actualPlan, totalPlans, `Migrando servico ${servico.clientes_id} - ${servico.login}`);
    // }
    // console.log('');
}
