import { base as ModelBase, Internetplanos } from "powerjs/db/models";
import conn from "powerjs/db/conn";
import Groups from "../models/Groups";
import console_tools from "powerjs/support/console_tools";

export function clear()
{
    conn.setDb(ModelBase.getConnection('duobox'));
    console.log('Limpando planos...');
    try {
        conn.beginTransaction();
        conn.exec('SET FOREIGN_KEY_CHECKS = OFF');
        conn.exec('TRUNCATE internetplanos');
        conn.exec('SET FOREIGN_KEY_CHECKS = ON');
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado planos: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabela de planos truncada');
}

export function migra() {
    let planos: Groups[] = Groups.all('id > 0');
    let totalPlanos = planos.length;
    let planoAtual = 0;
    planos.forEach((plano) => {
        console_tools.mostraProgresso(++planoAtual, totalPlanos, `Migrando plano ${plano.id} - ${plano.name}`);
        let planoNovo = new Internetplanos();
        planoNovo.id = plano.id;
        planoNovo.nome = plano.name;
        planoNovo.velocidade_down = plano.bandwidth_user_down;
        planoNovo.velocidade_down_garantia = plano.bandwidth_user_down;
        planoNovo.velocidade_up = plano.bandwidth_user_up;
        planoNovo.velocidade_up_garantia = plano.bandwidth_user_up;
        planoNovo.prioridade = plano.bandwidth_user_down_prio;
        planoNovo.nome_pool = '';
        planoNovo.descricao_nota_fiscal = planoNovo.nomePublico ?? plano.name;
        planoNovo.valor_scm = 0;
        planoNovo.valor_sva = 0;
        planoNovo.valor_servico = 0;
        planoNovo.valor_iva = 0;
        planoNovo.instrucoes_boleto = '';

        planoNovo.ciscoiosxe_nome_pool = '';
        planoNovo.ciscoiosxe_nome_pool_v6 = '';

        planoNovo.horario_ciscoiosxe_nome_pool = '';
        planoNovo.horario_ciscoiosxe_nome_pool_v6 = '';
        planoNovo.franquia_dados_ciscoiosxe_nome_pool = '';
        planoNovo.franquia_dados_ciscoiosxe_nome_pool_v6 = '';
        planoNovo.suspensao_parcial_ciscoiosxe_nome_pool = '';
        planoNovo.suspensao_parcial_ciscoiosxe_nome_pool_v6 = '';

        planoNovo.ciscoios_nome_pool = '';
        planoNovo.horario_ciscoios_nome_pool = '';
        planoNovo.franquia_dados_ciscoios_nome_pool = '';
        planoNovo.suspensao_parcial_ciscoios_nome_pool = '';

        planoNovo.huaweine_nome_pool = '';
        planoNovo.huaweine_nome_pool_v6 = '';
        planoNovo.horario_huaweine_nome_pool = '';
        planoNovo.horario_huaweine_nome_pool_v6 = '';
        planoNovo.franquia_dados_huaweine_nome_pool = '';
        planoNovo.franquia_dados_huaweine_nome_pool_v6 = '';
        planoNovo.suspensao_parcial_huaweine_nome_pool = '';
        planoNovo.suspensao_parcial_huaweine_nome_pool_v6 = '';

        planoNovo.huaweine_dns1 = '';
        planoNovo.huaweine_dns2 = '';

        planoNovo.junipermx_nome_pool = '';
        planoNovo.junipermx_nome_pool_v6 = '';
        planoNovo.horario_junipermx_nome_pool = '';
        planoNovo.horario_junipermx_nome_pool_v6 = '';
        planoNovo.franquia_dados_junipermx_nome_pool = '';
        planoNovo.franquia_dados_junipermx_nome_pool_v6 = '';
        planoNovo.suspensao_parcial_junipermx_nome_pool = '';
        planoNovo.suspensao_parcial_junipermx_nome_pool_v6 = '';

        planoNovo.lista_dinamica = '';

        if (!planoNovo.prioridade)
        {
            planoNovo.prioridade = 8;
        }

        planoNovo.burst_down = 0;
        planoNovo.burst_down_threshold = 0;
        planoNovo.burst_down_time = 0;
        planoNovo.burst_up = 0;
        planoNovo.burst_up_threshold = 0;
        planoNovo.burst_up_time = 0;

        // if (plano.burst == 'sim') {
        //     planoNovo.burst_up = plano.maxup;
        //     planoNovo.burst_up_threshold = plano.desaup;
        //     planoNovo.burst_up_time = plano.tempoup;
        //     planoNovo.burst_down = plano.maxdown;
        //     planoNovo.burst_down_threshold = plano.desadown;
        //     planoNovo.burst_down_time = plano.tempodown;
        // }

        planoNovo.suspensao_parcial_velocidade_down = 1;
        planoNovo.suspensao_parcial_velocidade_down_garantia = 1;
        planoNovo.suspensao_parcial_velocidade_up = 1;
        planoNovo.suspensao_parcial_velocidade_up_garantia = 1;
        planoNovo.suspensao_parcial_prioridade = 8;
        planoNovo.suspensao_parcial_nome_pool = '';
        planoNovo.suspensao_parcial_lista_dinamica = '';
        planoNovo.suspensao_parcial_burst_down = 0;
        planoNovo.suspensao_parcial_burst_down_threshold = 0;
        planoNovo.suspensao_parcial_burst_down_time = 0;
        planoNovo.suspensao_parcial_burst_up = 0;
        planoNovo.suspensao_parcial_burst_up_threshold = 0;
        planoNovo.suspensao_parcial_burst_up_time = 0;
//            if (plano.late_payment_reduce) {
//                planoNovo.suspensao_parcial_velocidade_down = plano.late_payment_speed_down;
//                planoNovo.suspensao_parcial_velocidade_down_garantia = plano.late_payment_speed_down;
//                planoNovo.suspensao_parcial_velocidade_up = plano.late_payment_speed_up;
//                planoNovo.suspensao_parcial_velocidade_up_garantia = plano.late_payment_speed_up;
//                planoNovo.suspensao_parcial_prioridade = 8;
//            }

        planoNovo.horario_velocidade_down = 1;
        planoNovo.horario_velocidade_down_garantia = 1;
        planoNovo.horario_velocidade_up = 1;
        planoNovo.horario_velocidade_up_garantia = 1;
        planoNovo.horario_prioridade = 8;
        planoNovo.horario_nome_pool = '';
        planoNovo.horario_lista_dinamica = '';
        planoNovo.horario_burst_down = 0;
        planoNovo.horario_burst_down_threshold = 0;
        planoNovo.horario_burst_down_time = 0;
        planoNovo.horario_burst_up = 0;
        planoNovo.horario_burst_up_threshold = 0;
        planoNovo.horario_burst_up_time = 0;

        planoNovo.franquia_dados_ativada = 0;
        planoNovo.franquia_dados = 1024;
        planoNovo.franquia_dados_velocidade_down = 1;
        planoNovo.franquia_dados_velocidade_down_garantia = 1;
        planoNovo.franquia_dados_velocidade_up = 1;
        planoNovo.franquia_dados_velocidade_up_garantia = 1;
        planoNovo.franquia_dados_prioridade = 8;
        planoNovo.franquia_dados_nome_pool = '';
        planoNovo.franquia_dados_lista_dinamica = '';
        planoNovo.franquia_dados_burst_down = 0;
        planoNovo.franquia_dados_burst_down_threshold = 0;
        planoNovo.franquia_dados_burst_down_time = 0;
        planoNovo.franquia_dados_burst_up = 0;
        planoNovo.franquia_dados_burst_up_threshold = 0;
        planoNovo.franquia_dados_burst_up_time = 0;

        planoNovo.desconto_antecipacao = 0;
        planoNovo.valor = plano.valorMensal;

        try {
            planoNovo.Insert();
        }
        catch (e) {
            console.log(e.params);
            var_dump(e.message ?? e.getMessage());
            let type;
            let params = '';
            e.params.forEach((value, index) => {
                console.log(index,value);
                if (index == 0) {
                    type = value.split('');
                }
                else {
                    console.log('type', type[index-1]);
                    if (value == null) {
                        params += 'null,';
                    }
                    else if (type[index-1] == 's') {
                        params += '"'+value+'",';
                    }
                    else {
                        params += value+',';
                    }
                }
            })
            console.log(params);
            exit();
        }
    });
    console.log();
}
