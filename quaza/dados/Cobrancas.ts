import {
    base as ModelBase,
    ClientesHasInternetplanos,
    Cobrancas,
    CobrancasBoleto,
    CobrancasParcelas
} from "powerjs/db/models";
import conn from "powerjs/db/conn";
import { data } from "powerjs/support/helper";
import console_tools from "powerjs/support/console_tools";
import codigobarra from "powerjs/cobrancas/boletos/codigobarra";
import ThreadManager from "powerjs/ThreadManager";
import { dbConnect, baseNamespace } from "../common"
import { quaza as config } from "../../config.json";
import Financeiro from "../models/Financeiro";

let listaBancos: any;

export function clear() {
    conn.setDb(ModelBase.getConnection('duobox'));
    console.log('Limpando cobrancas...');
    try {
        conn.beginTransaction();
        conn.disableFk();
        conn.exec('TRUNCATE cobrancas');
        conn.exec('TRUNCATE cobrancas_boleto');
        conn.exec('TRUNCATE cobrancas_parcelas');
        conn.exec('DELETE FROM contas_movimentacao WHERE documento_tipo = "BOLETO"');
        conn.enableFk();
        conn.commitTransaction();
    } catch (err) {
        conn.rollback();
        console.log(`Erro limpado cobrancas: ${err.getMessage()}`);
        exit();
    }
    console.log('Tabelas de cobrancas truncadas');
}


function connectDb()
{
    conn.createNewConnection();
    dbConnect(config.database);
}

function disconnectDb()
{
    let connIspbox = ModelBase.getConnection('duobox');
    let connQuaza = ModelBase.getConnection(baseNamespace);

    connQuaza.Disconnect();
    connIspbox.Disconnect();
}

function migraLote(pagina, linhasPagina, consoleBaseLine) {
    connectDb();
    pagina = parseInt(pagina)-1;
    let limitStart = pagina * linhasPagina;
    let limitEnd = (pagina+1) * linhasPagina;
    let offsetInc = 500;
    let linhaConsole = parseInt(consoleBaseLine) + (pagina * 2) + 1;
    conn.setDb(ModelBase.getConnection(baseNamespace));
    let first;
    try {
        first = conn.first(`SELECT COUNT(*) AS total
                            FROM (SELECT id
                                  FROM financeiro
                                  LIMIT ${linhasPagina} OFFSET ${limitStart}) AS a`);
    } catch (e) {
        console.log(e);
        exit();
    }

    let rowTotal = parseInt(first.total);
    let rowCount = 0;

    let financeiroTipos = {
        '0': 1, //mensalidade
        '1': 3, // Outro
        '2': 2, // Serviço
        '3': 3, // Voip
        '4': 6, // Venda
        default: 3
    }

    while (limitStart < limitEnd) {
        // verificamos se estamos no final do lote e ajustamos o offsetInc para não pegar registros da página seguinte
        if ((limitStart+offsetInc) > limitEnd) {
            offsetInc -= limitStart+offsetInc-limitEnd;
        }

        let lancamentos: Financeiro[];
        try {
            lancamentos = Financeiro.all(`1=1 ORDER BY id ASC LIMIT ${offsetInc} OFFSET ${limitStart}`);
        } catch (e) {
            var_dump(e);
            exit();
        }

        let competenciaRegex = /(?<mes>[a-zA-Z]+)\/(?<ano>\d{4})/;
        let competenciaMeses = {
            'Janeiro': '01',
            'Fevereiro': '02',
            'Março': '03',
            'Abril': '04',
            'Maio': '05',
            'Junho': '06',
            'Julho': '07',
            'Agosto': '08',
            'Setembro': '09',
            'Outubro': '10',
            'Novembro': '11',
            'Dezembro': '12'
        };

        let linhaDigitavelRegex = new RegExp(/[0-9. ]{54}/);

        lancamentos.forEach((b) => {
            rowCount++;

            console_tools.mostraProgresso(rowCount, rowTotal, `Titulo ID: ${b.id}`, linhaConsole);
            let cobrancaBoleto = null;
            let cobrancaParcela = null;
            let cobranca = new Cobrancas();
            let cliente_id = b.cliente_id;
            // let servicoInternet = ClientesHasInternetplanos.first('clientes_id = ? ORDER BY ativo DESC, id ASC', [cliente_id]);
            cobranca.id = b.id;
            cobranca.clientes_id = cliente_id;
            // existem boletos vinculados com contas que não existem mais
            // então se o boleto está vinculado com uma conta inexistente
            // jogamos para a conta 1 do ispbox que é a cobrança em carteira
            cobranca.bancos_id = b.idCarteira ? parseInt(b.idCarteira)+1 : 1;
            cobranca.origem_tipo = 'INTERNET';
            cobranca.origem_id = b.idContrato;

            cobranca.valor = b.reg_valor;
            cobranca.data_vencimento = b.reg_vencimento;
            cobranca.tipo = financeiroTipos[b.tipo] || financeiroTipos.default;

            cobranca.valor_mensalidade = 0;

            cobranca.metodo_cobranca = Cobrancas.METODO_BOLETO;

            cobranca.nossonumero = '0';
            cobranca.sequencial = 0;
            //
            // var_dump();
            // exit();
            if (b.linhaDigitavel && linhaDigitavelRegex.test(b.linhaDigitavel)) {
                let parse = codigobarra.parseBancoLinhaDigitavel(b.linhaDigitavel);
                cobranca.nossonumero = parse.getNossoNumero() + parse.getDvNossoNumero();
                cobranca.sequencial = parse.getSequencial();
                // parse.setVencimento(cobranca.getDataVencimento);
                // parse.setValorDocumento(cobranca.getValor);
                // b.codigo_barra_transacao = parse.getLinhaDigitavel();
            }

            cobranca.referencia_mensalidade = null;
            cobranca.referencia_mensalidade_inicio = null;
            cobranca.referencia_mensalidade_fim = null;
            cobranca.registrado = 1;
            cobranca.registro_remetido = 1;
            cobranca.remetido = 1;

            // cobranca.gerencianet_cobranca_chave = b.chave_gnet ?? '';
            // cobranca.transacao_id = b.chave_gnet2 ?? b.chave_juno ?? '';
            cobranca.transacao_id = b.id_transacao;

            cobranca.data_gerado = b.data_entrada;
            cobranca.mensagem = b.obs;

            cobranca.status = 0;
            if (b.reg_deleted == 1) {
                cobranca.status = 2;
            }
            else if (b.bx_pagamento != '' && b.bx_pagamento != '0000-00-00 00:00:00') {
                    cobranca.status = 1;
            }
            cobranca.data_pagamento = (cobranca.status == Cobrancas.STATUS_PAGO) ? b.bx_pagamento : null;
            cobranca.valor_pago = (cobranca.status == Cobrancas.STATUS_PAGO) ? b.bx_valor_pago : 0;


            cobrancaBoleto = new CobrancasBoleto();
            cobrancaBoleto.id = cobranca.id;
            cobrancaBoleto.linha_digitavel = b.linhaDigitavel ?? '';
            cobrancaBoleto.vencimento_contra_apresentacao = 0;

            cobranca.cobrancas_boleto_id = cobranca.id;
            cobranca.desconto_antecipacao = b.descontoAntecipacao;

            if (cobranca.tipo == Cobrancas.TIPO_MENSALIDADE) {
                // cobranca.mensagem = '';
                // cobranca.tipo = 1;
                cobranca.valor_mensalidade = cobranca.valor;

                let competencia = b.competencia.match(competenciaRegex);
                let competenciaMes = competenciaMeses[competencia.groups.mes] ?? 0;
                let competenciaAno = competencia.groups.ano;
                let dataVencimento = b.getRegVencimento;
                let refMensalidade;

                if (competenciaMes == 0) {
                    refMensalidade = dataVencimento;
                }
                else {
                    refMensalidade = data.convertAnyToDateTime(`${competenciaAno}-${competenciaMes}-${dataVencimento.format('d')}`);
                }

                cobranca.referencia_mensalidade = refMensalidade.format('Y-m-01');

                cobranca.proporcional_mensalidade = data.getUltimoDiaMes(cobranca.getReferenciaMensalidade);
                cobranca.referencia_mensalidade_inicio = refMensalidade.format('Y-m-d');
                cobranca.referencia_mensalidade_fim = data.subDiasData(data.addMesData(refMensalidade), 1).format('Y-m-d');
                cobrancaParcela = new CobrancasParcelas();
                cobrancaParcela.cobrancas_id = cobranca.id;
                cobrancaParcela.tipo = 'MENSALIDADE';
                cobrancaParcela.valor = cobranca.valor_mensalidade;
                cobrancaParcela.mensalidade_inicio = cobranca.referencia_mensalidade_inicio;
                cobrancaParcela.mensalidade_fim = cobranca.referencia_mensalidade_fim;
            }

            // // default não usados
            //
            try {
                if (cobrancaBoleto) {
                    cobrancaBoleto.setCriarLinhaDigitavel(false);
                    cobrancaBoleto.setLogSistema(false);
                    if (!cobrancaBoleto.Save()) {
                        throw 'Não foi possível salvar os dados do boleto';
                    }
                }
                cobranca.setLogSistema(false);
                cobranca.setCadastraMovimentacao(false);
                cobranca.setImportacao(true);
                cobranca.setSetSequencial(false);
                cobranca.setGeraNossoNumero(false);
                if (!cobranca.Insert()) {
                    throw 'Não foi possível salvar o boleto';
                }
                if (cobrancaParcela) {
                    cobrancaParcela.setLogSistema(false);
                    if (!cobrancaParcela.Save()) {
                        throw 'Não foi possível salvar os valores de parcelas da cobrança';
                    }
                }
            } catch (e) {
                // if (e.message) {
                //     console.log(e.message);
                // } else {
                    console.log(e.params);
                    var_dump(e.message ?? e.getMessage());
                    let type;
                    let params = '';
                    e.params.forEach((value, index) => {
                        console.log(index,value);
                        if (index == 0) {
                            type = value.split('');
                        }
                        else {
                            console.log('type', type[index-1]);
                            if (value == null) {
                                params += 'null,';
                            }
                            else if (type[index-1] == 's') {
                                params += '"'+value+'",';
                            }
                            else {
                                params += value+',';
                            }
                        }
                    })
                    console.log(params);
                    // var_dump(err);
                    console.log(`[${pagina}] Erro inserindo: ${cobranca.id} - ${cobrancaBoleto.id}`)
                // }
                exit();
            }

        });
        limitStart += offsetInc;
        gc();
        php.gc_collect_cycles();


    }
    // disconnectDb();
    // return actualCharge;
    return true;

}

export function migra() {
    console.log('Verificando quantidade de cobrancas a serem migradas...');
    let rowCount = Financeiro.rowCount();
    if (rowCount === false) {
        console.log('Erro ao tentar localizar as cobrancas')
        exit();
    }

    let threads = ThreadManager.getCpuCount();
    // let threads = 1;

    // @ts-ignore
    let rowsPerThread = Math.ceil(rowCount / threads);

    console.log(`${rowCount} cobranças encontradas`);

    // listaBancos = FinanceiroPortador.toList('id', 'id');

    let pm = new ThreadManager(threads);
    console.log('iniciando childs...');

    let consoleBaseLine = console_tools.getCursorPosition()[0];

    for (let threadNum = 1; threadNum <= threads; threadNum++) {
            pm.startProcess(function (thisProcess: ThreadManager, pagina, linhasPagina, consBaseLine) {
                // o fork precisa de uma nova conexão com o banco de dados
                connectDb();
                migraLote(pagina, linhasPagina, consBaseLine);
            }, threadNum, rowsPerThread, consoleBaseLine);
    }
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log('');
    console.log("Esperando todos os childs concluirem...");
    pm.waitChildsFinish();
    // o processo principal precisa restabelecer as conexões com o banco de dados
    // connectDb();
    console_tools.setCursorPosition(consoleBaseLine+(threads*2)+2);
}