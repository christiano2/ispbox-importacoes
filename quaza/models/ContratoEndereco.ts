import { QuazaModel } from "./QuazaModel";

class ContratoEndereco extends QuazaModel {
    static table = 'contratoEndereco';
    static {
        this.initQueue();
    }
}

export default ContratoEndereco;
export { ContratoEndereco };