import { QuazaModel } from "./QuazaModel";

class ContratoServicoContrato extends QuazaModel {
    static table = 'contratoServicoContrato';
    static relations: Record<string, any> = {
    //     belongsTo: {
    //         ClientesHasInternetplanos: {
    //             class: 'duobox\\Models\\ClientesHasInternetplanos',
    //             foreignKey: 'login',
    //             parentKey: 'pppoe_login'
    //         }
    //     }
    }
    static {
        this.initQueue();
    }
}

export default ContratoServicoContrato;
export { ContratoServicoContrato };