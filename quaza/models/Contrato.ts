import { QuazaModel } from "./QuazaModel";
import ContratoEndereco from "./ContratoEndereco";
import ContratoLogin from "./ContratoLogin";
import ContratoServicoContrato from "./ContratoServicoContrato";

class Contrato extends QuazaModel {
    static relations: Record<string, any> = {
    //     belongsTo: {
    //         ClientesHasInternetplanos: {
    //             class: 'duobox\\Models\\ClientesHasInternetplanos',
    //             foreignKey: 'login',
    //             parentKey: 'pppoe_login'
    //         }
    //     }
        hasOne: {
            ContratoEndereco: {
                class: ContratoEndereco.className(),
                foreignKey: 'id'
            },
            ContratoLogin: {
                class: ContratoLogin.className(),
                foreignKey: 'idContrato'
            },
            ContratoServicoContrato: {
                class: ContratoServicoContrato.className(),
                foreignKey: 'idContrato'
            }
        }
    }
    static {
        this.initQueue();
    }
}

export default Contrato;
export { Contrato };