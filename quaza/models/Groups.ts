import { QuazaModel } from "./QuazaModel";

class Groups extends QuazaModel {
    static relations: Record<string, any> = {
    }

    static {
        this.initQueue();
    }
}

export default Groups;
export { Groups };