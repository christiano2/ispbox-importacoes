import { QuazaModel } from "./QuazaModel";
import Clientes from "./Clientes";
import FinanceiroGateway from "./FinanceiroGateway";

class Financeiro extends QuazaModel {
    static relations: Record<string, any> = {
        belongsTo: {
            Clientes: {
                class: Clientes.className(),
                foreignKey: 'cliente_id'
            }
        },
        hasOne: {
            FinanceiroGateway: {
                class: FinanceiroGateway.className(),
                foreignKey: 'idFinanceiro'
            }
        }
    }

    static {
        this.initQueue();
    }
}

export default Financeiro;
export { Financeiro };