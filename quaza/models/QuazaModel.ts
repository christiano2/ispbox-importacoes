import { Model } from "../../lib/model";

class QuazaModel extends Model {
    public static namespace = 'Quaza';
}

export default QuazaModel;
export { QuazaModel };
