import { QuazaModel } from "./QuazaModel";

class FinanceiroGateway extends QuazaModel {
    static table = 'financeiroGateway';
    static relations: Record<string, any> = {
    }

    static {
        this.initQueue();
    }
}

export default FinanceiroGateway;
export { FinanceiroGateway };