import { QuazaModel } from "./QuazaModel";
import ContratoLoginEndereco from "./ContratoLoginEndereco";
import Groups from "./Groups";

class ContratoLogin extends QuazaModel {
    static table = 'contratoLogin';
    static relations: Record<string, any> = {
        belongsTo: {
            Groups: {
                class: Groups.className(),
                foreignKey: 'idGroup'
            }
        },
        hasOne: {
            ContratoLoginEndereco: {
                class: ContratoLoginEndereco.className(),
                foreignKey: 'id'
            },
        }
    }
    static {
        this.initQueue();
    }
}

export default ContratoLogin;
export { ContratoLogin };