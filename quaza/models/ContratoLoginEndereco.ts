import { QuazaModel } from "./QuazaModel";

class ContratoLoginEndereco extends QuazaModel {
    static table = 'contratoLoginEndereco';
    static {
        this.initQueue();
    }
}

export default ContratoLoginEndereco;
export { ContratoLoginEndereco };