import { QuazaModel } from "./QuazaModel";
import Contrato from "./Contrato";

class Clientes extends QuazaModel {
    // Logins: Logins[];
    // Financeiro: Financeiro[];
    static relations: Record<string, any> = {
    //     belongsTo: {
    //         Plano: {
    //             class: Planos.className(),
    //             foreignKey: 'plano',
    //             parentKey: 'nome'
    //         }
    //     },
        hasMany: {
            Contrato: {
                class: Contrato.className(),
                foreignKey: 'idCliente'
            },
            // Financeiro: {
            //     class: Financeiro.className(),
            //     foreignKey: 'cod_cliente',
            //     parentKey: 'cod_cliente'
            // }
        }
    }

    static {
        this.initQueue();
    }
}

export default Clientes;
export { Clientes };
