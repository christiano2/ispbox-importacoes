#!/usr/bin/env powerjs
// import conn from "powerjs/db/conn";
// import models from "powerjs/db/models";
import * as Voalle from "./voalle/index";
import * as mkauth from "./mkauth/mkauth";
import * as radius from "./lib/ispbox/Radius";
import * as sgp from "./sgp/index";
import * as altarede from "./altarede/index";
import * as quaza from "./quaza/index";
import * as bancos from "./lib/ispbox/Bancos";

// const database = {
//     login: 'root',
//     password: 'master123',
//     host: '172.16.107.1',
//     dbname: 'ispbox_zcnet'
// }
// const database = {
//     login: 'root',
//     password: 'master123',
//     host: '172.16.107.1',
//     dbname: 'ispbox_speednet'
// }
//
// const mysqlconn = conn.newADOConnection(`mysqli://${database.login}:${database.password}@${database.host}/${database.dbname}`);

// configuramos todos os models no namespace da duobox para usar a nova conexão como padrão
// models.addConnection("duobox", mysqlconn);
// models.addConnection("", mysqlconn);

// sgp.run();
// altarede.run();

quaza.run();

// Voalle.run();
// mkauth.run();
// radius.cria();
// bancos.atualizaSequenciais();
